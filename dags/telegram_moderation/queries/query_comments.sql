WITH full_comments AS (
    SELECT
        dcc.id as comments_id,
        TO_CHAR(
            dcc.created_at AT TIME ZONE 'UTC',
            'DD-MM-YYYY HH24:MI:SS'
        ) AS created_at,
        DATE_TRUNC('second', dcc.updated_at AT TIME ZONE 'UTC') AS comments_update,
        TO_CHAR(
            DATE_TRUNC('second', dcc.updated_at AT TIME ZONE 'UTC'),
            'DD-MM-YYYY HH24:MI:SS'
        ) AS updated_at,
        COALESCE(
            CASE
                WHEN dpp.body ->> 'pt-BR' ~ '<xml>' THEN E'\n\n<strong>Breve descrição:</strong> ' || array_to_string(
                    xpath(
                        '//div[@class="brief_description"]/text()',
                        xmlparse(document dpp.body ->> 'pt-BR')
                    ),
                    ''
                ) || E'\n\n<strong>Observações:</strong> ' || array_to_string(
                    xpath(
                        '//div[@class="observations"]/text()',
                        xmlparse(document dpp.body ->> 'pt-BR')
                    ),
                    ''
                )
                ELSE REGEXP_REPLACE(
                    REGEXP_REPLACE(
                        dcc.body ->> 'pt-BR',
                        '^{"pt-BR": "(.*)"}*$',
                        '\1'
                    ),
                    '</?(em|p|strong)>',
                    '',
                    'g'
                )
            END,
            dcc.body ->> 'pt-BR'
        ) AS body,
        CONCAT(
            'https://brasilparticipativo.presidencia.gov.br/processes/',
            dppc.slug,
            '/f/',
            dpp.decidim_component_id,
            '/proposals/',
            dpp.id,
            '?commentId=',
            dcc.id
        ) AS comment_link,
        dpp.title ->> 'pt-BR' AS title,
        ds.name ->> 'pt-BR' AS name_category,
        dppc.group_chat_id,
        dppc.id as participatory_space_id
    from
        public.decidim_comments_comments dcc
        INNER join public.decidim_proposals_proposals dpp on dcc.decidim_root_commentable_id = dpp.id
        INNER JOIN public.decidim_components dc ON dc.id = dpp.decidim_component_id
        INNER JOIN public.decidim_participatory_processes dppc ON dppc.id = dc.participatory_space_id
        INNER JOIN public.decidim_participatory_process_steps dpps ON dpps.decidim_participatory_process_id = dc.participatory_space_id
        AND dpps.active = TRUE
        LEFT JOIN public.decidim_scopes ds ON ds.id = dpp.decidim_scope_id
    WHERE
        dppc.group_chat_id IS NOT NULL
        AND dppc.group_chat_id <> ''
        AND dc.manifest_name in ('proposals', 'meetings', 'surveys')
        AND dppc.end_date >= current_date
        AND dc.published_at IS NOT NULL
),
message_comments AS (
    SELECT
        fc.*,
        '💬💬💬 Comentário em ' || TO_CHAR(fc.comments_update, 'DD/MM/YYYY "às" HH24:MI') || E'\n\n<a href="' || fc.comment_link || E'">Acesse aqui</a>' || E'\n\n<strong>Categoria:</strong> ' || COALESCE(fc.name_category, '-') ||
        E'\n\n<strong>Proposta:</strong> ' || REPLACE(COALESCE(fc.title, '-'), '"', '') ||
        E'\n\n<strong>Comentário:</strong> ' || REPLACE(COALESCE(fc.body, '-'), '"', '') || E'\n' AS message_telegram
    FROM
        full_comments fc
)
SELECT
    group_chat_id,
    comments_update,
    message_telegram
FROM
    message_comments
WHERE
    comments_update > %(last_update_time)s
    AND participatory_space_id = %(processes_id)s
ORDER BY
    comments_update ASC;
