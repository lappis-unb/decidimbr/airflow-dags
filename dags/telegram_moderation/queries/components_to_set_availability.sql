select
	processes.id as processes_ids,
	processes.slug as processes_slugs,
	processes.group_chat_id as telegram_group_chat_id,
	components.settings,
	steps.id as step_id,
	steps.title,
	components.settings->'steps'->cast(steps.id as text)->'endorsements_enabled',
	cast(components.settings->'global'->'participatory_texts_enabled' as bool) as is_texto_participativo,
	INITCAP(replace(cast(components."name"->'pt-BR' as text), '"', '')) as nome_do_componente,
	'https://brasilparticipativo.presidencia.gov.br/admin/participatory_processes/' || processes.slug || '/components/' || components.id || '/edit/' as admin_url
from
	decidim_participatory_processes processes
inner join decidim_components components on
	processes.id = components.participatory_space_id
inner join (select * from decidim_participatory_process_steps dpps where dpps.active = true) as steps on steps.decidim_participatory_process_id = components.participatory_space_id
where
	processes.group_chat_id notnull
	and processes.group_chat_id <> ''
	and components.manifest_name = 'proposals'
	and processes.end_date >= current_date
	and processes.id not in (4)
	and components.published_at notnull
