WITH state_map AS (
    SELECT 'accepted' AS state, 'Aceita ' AS label, '✅ ✅ ✅' AS emoji UNION ALL
    SELECT 'evaluating' AS state, 'Em avaliação ' AS label, '📥 📥 📥' AS emoji UNION ALL
    SELECT 'withdrawn' AS state, 'Retirada ' AS label, '🚫 🚫 🚫' AS emoji UNION ALL
    SELECT 'rejected' AS state, 'Rejeitada ' AS label, '⛔ ⛔ ⛔' AS emoji UNION ALL
    SELECT 'others' AS state, 'Atualizada ' AS label, '🔄 🔄 🔄' AS emoji UNION ALL
    SELECT 'new' AS state, '' AS label, '📣 📣 📣 <b>[NOVA]</b>' AS emoji
),
full_proposals AS (
    SELECT
        dpp.id AS proposals_id,
        dpp.decidim_component_id AS component_id,
        dc.participatory_space_id,
        dppc.group_chat_id,
        dpp.created_at AT TIME ZONE 'UTC' AS created_at,
        DATE_TRUNC('second', dpp.updated_at AT TIME ZONE 'UTC') AS updated_at,
        COALESCE(dpp.state, 'others') AS state,
        dpp.title->>'pt-BR' AS title,
        COALESCE(
            CASE
                WHEN dpp.body->>'pt-BR' ~ '<xml>' THEN
                    E'\n\n<strong>Breve descrição:</strong> ' || array_to_string(
                        xpath('//div[@class="brief_description"]/text()', xmlparse(document dpp.body->>'pt-BR')), ''
                    ) || E'\n\n<strong>Observações:</strong> ' || array_to_string(
                        xpath('//div[@class="observations"]/text()', xmlparse(document dpp.body->>'pt-BR')), ''
                    )
                ELSE
                    REGEXP_REPLACE(
                        REGEXP_REPLACE(
                            dpp.body->>'pt-BR',
                            '^{"pt-BR": "(.*)"}*$',
                            '\1'
                        ),
                        '</?(em|p|strong)>',
                        '',
                        'g'
                    )
            END,
            dpp.body->>'pt-BR'
        ) AS body,
        CONCAT(
            'https://brasilparticipativo.presidencia.gov.br/processes/',
            dppc.slug, '/f/', dpp.decidim_component_id, '/proposals/', dpp.id
        ) AS proposal_link,
        ds.name->>'pt-BR' AS name_category
    FROM
        public.decidim_proposals_proposals dpp
    INNER JOIN public.decidim_components dc ON dc.id = dpp.decidim_component_id
    INNER JOIN public.decidim_participatory_processes dppc ON dppc.id = dc.participatory_space_id
    INNER JOIN public.decidim_participatory_process_steps dpps ON
        dpps.decidim_participatory_process_id = dc.participatory_space_id AND dpps.active = TRUE
    LEFT JOIN public.decidim_scopes ds ON ds.id = dpp.decidim_scope_id
    WHERE
        dppc.group_chat_id IS NOT NULL AND dppc.group_chat_id <> ''
        AND dc.manifest_name = 'proposals'
        AND dppc.end_date >= current_date
        and dc.settings->'global'->'participatory_texts_enabled' = 'false'
        AND dc.published_at IS NOT NULL
),
message_proposals AS (
    SELECT
        fp.*,
        COALESCE(mp.emoji, '') || ' ' ||
        COALESCE(mp.label, '') || 'Proposta em ' || TO_CHAR(fp.updated_at, 'DD/MM/YYYY "às" HH24:MI') || E'\n\n<a href="' || fp.proposal_link || E'">Acesse aqui</a>' ||
        E'\n\n<strong>Categoria:</strong> ' || COALESCE(fp.name_category, '-') ||
        E'\n\n<strong>Proposta:</strong> ' || REPLACE(COALESCE(fp.title, '-'), '"', '') ||
        E'\n\n<strong>Descrição:</strong> ' || REPLACE(COALESCE(fp.body, '-'), '"', '') || E'\n' AS message_telegram
    FROM
        full_proposals fp
    LEFT JOIN state_map mp ON mp.state =
        case
		when DATE_TRUNC('second', fp.created_at) at TIME zone 'UTC' < DATE_TRUNC('second', fp.updated_at) at TIME zone 'UTC' then fp.state
		else 'new'
	end
)
SELECT
    group_chat_id,
    updated_at,
    message_telegram
FROM
    message_proposals
WHERE
    updated_at > %(last_update_time)s
    AND participatory_space_id = %(processes_id)s
ORDER BY
    updated_at ASC;
