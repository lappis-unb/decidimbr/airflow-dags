# pylint: disable=import-error, invalid-name, expression-not-assigned

import logging
import re
from collections import defaultdict
from datetime import timedelta
from functools import partial
from io import StringIO
from pathlib import Path
from urllib.parse import urljoin

import bs4
import pandas as pd
import requests
from airflow.decorators import dag, task
from airflow.hooks.base import BaseHook
from airflow.models.connection import Connection
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.providers.telegram.hooks.telegram import TelegramHook
from bs4 import BeautifulSoup
from sqlalchemy import create_engine

BP_WEB = "bp_api"
BP_DB = "bp_replica"
CONFIG_DB = "pg_bp_analytics"

SSH_CONN = "ssh_tunnel_decidim"

TELEGRAM_CONN_ID = "telegram_moderation"

PAGE_FORM_CLASS = "form edit_component"


class GraphQLHook(BaseHook):
    """Uma classe para autenticação com uma API GraphQL usando o Apache Airflow.

    Esta classe estende a `BaseHook` do Apache Airflow e fornece métodos
    para executar consultas GraphQL com autenticação.

    Attributes:
    ----------
        conn_id (str): O ID de conexão configurado no Airflow, usado para obter
            as credenciais de autenticação.
        api_url (str): A URL base da API GraphQL.
        auth_url (str): A URL para autenticação na API GraphQL.
        payload (dict): Um dicionário contendo o email e a senha para autenticação.

    Methods:
    -------
        __init__(conn_id: str):
            Inicializa a instância do GraphQLHook com um ID de conexão.

        get_session() -> requests.Session:
            Cria e retorna uma sessão autenticada usando as credenciais da conexão.
    """

    def __init__(self, conn_id: str):
        """
        Initializes the GraphQLHook instance.

        Args:
        ----
            conn_id (str): The connection ID used for authentication.
        """
        assert isinstance(conn_id, str), "Param type of conn_id has to be str"

        conn_values = self.get_connection(conn_id)
        assert isinstance(conn_values, Connection), "conn_values was not created correctly."

        self.conn_id = conn_id
        self.api_url = conn_values.host
        self.auth_url = urljoin(self.api_url, "api/sign_in")
        self.payload = {
            "user[email]": conn_values.login,
            "user[password]": conn_values.password,
        }

    def get_session(self) -> requests.Session:
        """
        Creates a requests session authenticated with the provided user credentials.

        Returns
        -------
            requests.Session: Authenticated session object.
        """
        session = requests.Session()

        try:
            r = session.post(self.auth_url, data=self.payload, verify=True)
            r.raise_for_status()
        except requests.exceptions.HTTPError as e:
            logging.info("A login error occurred: %s", e)
            raise e
        return session


def _convert_html_form_to_dict(html_form: bs4.element.Tag) -> defaultdict:
    """Convert html <form> and <input> tags to python dictionary.

    Args:
    ----
        html_form (bs4.element.Tag): beautiful soup object with
            respective html <form> filtered.

    Returns:
    -------
        defaultdict: a dictionary of lists with html input tag name
        and value.
    """
    dict_output = defaultdict(list)
    for tag in html_form.find_all("input"):
        if tag.get("type", None) == "checkbox":
            if tag.get("checked", None):
                dict_output[tag["name"]].append(tag["value"])
        else:
            dict_output[tag["name"]].append(tag["value"])

    return dict_output


def _find_form_input_id(dict_form: bs4.element.Tag, pattern: str):
    """Find a form input id using regex.

    Args:
    ----
        dict_form (bs4.element.Tag): a dict contains beautiful soup objects
            with respective html <form> filtered.

    Returns:
    -------
        form_input_id: a string with form input id value.

    Raises:
    ------
        IndexError: If does not found a component of creation enabled.
    """
    logging.info("Matching with %s", str(pattern))
    pattern_match = [component for component in dict_form if re.match(pattern, component)]
    logging.info("Found %s checkboxes", pattern_match)
    pattern_match = sorted(pattern_match)

    return pattern_match


def set_comment_permmision(dict_form: dict, active_step: int, status: bool):
    comments_pattern = r"component\[.*step_settings\](\[" + str(active_step) + r"\]){0,1}\[comments_blocked\]"
    comments_form_input_id = _find_form_input_id(dict_form, comments_pattern)
    for checkbox in comments_form_input_id:
        dict_form[checkbox] = [f"{int(not status)}"]
        logging.warning("Set %s as: %s", checkbox, dict_form[checkbox])


def set_proposals_permmision(dict_form: dict, active_step: int, status: bool):
    proposals_pattern = (
        r"component\[.*step_settings\](\[" + str(active_step) + r"\]){0,1}\[creation_enabled\]"
    )
    proposals_form_input_id = _find_form_input_id(dict_form, proposals_pattern)
    for checkbox in proposals_form_input_id:
        dict_form[checkbox] = [f"{int(status)}"]
        logging.warning("Set %s as: %s", checkbox, dict_form[checkbox])


def set_availability(session: requests.Session, availability: bool, component: pd.Series):
    return_component_page = session.get(component["admin_url"])

    logging.info("Requested page form from %s", component["admin_url"])

    b = BeautifulSoup(return_component_page.text, "html.parser")
    html_form = b.find(class_=PAGE_FORM_CLASS)

    dict_form = _convert_html_form_to_dict(html_form)

    step_id = component["step_id"]
    if component["is_texto_participativo"]:
        set_comment_permmision(dict_form, step_id, availability)
    else:
        set_proposals_permmision(dict_form, step_id, availability)
        set_comment_permmision(dict_form, step_id, availability)

    session.post(component["admin_url"].rstrip("/").rstrip("edit/"), data=list(dict_form.items()))
    # r.raise_for_status()


QUERIES_FOLDER = Path(__file__).parent.joinpath("./queries/")

with open(QUERIES_FOLDER.joinpath("./components_to_set_availability.sql")) as file:
    SQL_QUERY = file.read()


def _get_sql_engine(db_conn, ssh_tunnel=None):
    db = PostgresHook.get_connection(db_conn)

    connection_string = f"postgresql://{db.login}:{db.password}@{db.host}:{db.port}/{db.schema}"
    engine = create_engine(connection_string)

    return engine


def _get_df_from_sql(query, db_conn, ssh_tunnel=None):
    engine = _get_sql_engine(db_conn, ssh_tunnel)
    table = pd.read_sql(query, engine, dtype=str)
    return table


doc_md = """
DAG to set proposals availability on decidim.

The result is the checkbox `Participantes podem criar propostas` marked as checked or not.

Exemple:

When setting decidim's proposals availability:

if permission_status is true

then the code `marking` checkbox sends

(component[step_settings][1][creation_enabled], 0)

(component[step_settings][1][creation_enabled], 1)

else

then the code `unmarking` checkbox sends

(component[step_settings][1][creation_enabled], 0)

## Permissions config truth table

| Comments | Proposals | Permissions Config |
| - | - | - |
| 0 | 0 | 0|
| 0 | 1 | 1|
| 1 | 0 | 2|
| 1 | 1 | 3|
"""


class DecidimNotifierDAGGenerator:  # noqa: D101
    def generate_dag(
        self,
        dag_id: str,
        schedule: str,
    ):
        default_args = {
            "owner": "Paulo G.",
            "start_date": "2024-10-25",
            "depends_on_past": False,
            "retries": 10,
            "retry_delay": timedelta(minutes=5),
            # "on_failure_callback": send_slack,
            # "on_retry_callback": send_slack, #! Change to telegram notifications.
        }

        @dag(
            dag_id=dag_id,
            default_args=default_args,
            schedule=schedule,
            catchup=False,
            doc_md=doc_md,
            tags=["bp", "moderation"],
            is_paused_upon_creation=True,
        )
        def notify_on_n_off_permissions(
            permission_status: bool,
        ):  # pylint: disable=missing-function-docstring
            # due to Airflow DAG __doc__

            @task
            def get_components_to_set_availability():
                components = _get_df_from_sql(SQL_QUERY, BP_DB, SSH_CONN).groupby("processes_ids")
                return [pd.DataFrame(component[1]).to_csv(index=False) for component in components]

            @task
            def set_permissions_availability(components_to_set_availability, availability: bool):
                """Airflow task that makes a request to set status of `Participantes podem criar propostas`.

                It means that a decidim component became available or unavailable
                to receive new proposals.

                Args:
                ----
                    permission_status (bool): the desired action on the html
                        input checkbox `Participantes podem criar propostas`.
                """
                session = GraphQLHook(BP_WEB).get_session()
                base_func = partial(set_availability, session, availability)

                df = pd.read_csv(StringIO(components_to_set_availability))
                df.apply(base_func, axis=1)

                session.close()

            @task
            def send_telegram(components_availability, availability: bool):
                """Airflow task to send telegram message.

                Args:
                ----
                    permission_status (bool): the desired action on the html
                        input checkbox `Participantes podem criar propostas`.
                """
                components = pd.read_csv(StringIO(components_availability))
                components = components.sort_values(by="nome_do_componente", ignore_index=True)
                components.reset_index(drop=True)

                unique_telegram_ids = pd.unique(components["telegram_group_chat_id"])
                assert len(unique_telegram_ids) == 1
                telegram_group_id = unique_telegram_ids[0]

                only_comments = "".join(
                    [
                        f"        • {name}\n"
                        for _, name in components[components["is_texto_participativo"]][
                            "nome_do_componente"
                        ].items()
                    ]
                )
                proposals_n_comments = "".join(
                    [
                        f"        • {name}\n"
                        for _, name in components[~components["is_texto_participativo"]][
                            "nome_do_componente"
                        ].items()
                    ]
                )

                message = "✅  <b>[ATIVADO]</b>\n" if availability else "🚫  <b>[DESATIVADO]</b>\n"

                message += (
                    f"\nPropostas e Comentarios:\n\n{proposals_n_comments}" if proposals_n_comments else ""
                )
                message += f"\nComentarios:\n\n{only_comments}" if only_comments else ""
                TelegramHook(
                    telegram_conn_id=TELEGRAM_CONN_ID,
                    chat_id=str(telegram_group_id),
                ).send_message(
                    api_params={
                        "text": message,
                    }
                )

            get_components_to_set_availability_task = get_components_to_set_availability()
            set_permissions_availability_task = set_permissions_availability.partial(
                availability=permission_status
            ).expand(components_to_set_availability=get_components_to_set_availability_task)

            send_telegram_task = send_telegram.partial(availability=permission_status).expand(
                components_availability=get_components_to_set_availability_task
            )

            set_permissions_availability_task >> send_telegram_task

        return notify_on_n_off_permissions


DecidimNotifierDAGGenerator().generate_dag(
    dag_id="set_on_availability",
    schedule="0 8 * * *",
)(True)

DecidimNotifierDAGGenerator().generate_dag(
    dag_id="set_off_availability",
    schedule="0 22 * * *",
)(False)
