import logging
from datetime import datetime, timedelta
from pathlib import Path
from typing import Any, Dict, List

import numpy as np
import pandas as pd
from airflow.decorators import dag, task
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.providers.telegram.hooks.telegram import TelegramHook
from sqlalchemy import create_engine

from plugins.telegram.decorators import telegram_retry

BP_DB = "bp_replica"
CONFIG_DB = "pg_bp_analytics"
SSH_CONN = "ssh_tunnel_decidim"
TELEGRAM_CONN_ID = "telegram_moderation"
TELEGRAM_MAX_RETRIES = 50

QUERIES_FOLDER = Path(__file__).parent.joinpath("./queries/")
with open(QUERIES_FOLDER.joinpath("query_proposals.sql")) as file:
    PROPOSALS_IN_PROCESSES_SQL = file.read()


def _get_sql_engine(db_conn, ssh_tunnel=None):
    db = PostgresHook.get_connection(db_conn)

    connection_string = f"postgresql://{db.login}:{db.password}@{db.host}:{db.port}/{db.schema}"
    engine = create_engine(connection_string)
    return engine


def _get_df_from_sql(query, db_conn, ssh_tunnel=None, params=None):
    engine = _get_sql_engine(db_conn, ssh_tunnel)
    try:
        table = pd.read_sql(query, engine, params=params, dtype=str)
    finally:
        engine.dispose()
    return table


@telegram_retry(max_retries=TELEGRAM_MAX_RETRIES)
def _send_telegram_notification(telegram_chat_id, telegram_topic_id, message):
    if not message:
        logging.warning("No text provided for Telegram message; skipping notification.")
        return

    message = message.replace("<br>", "")

    TelegramHook(
        telegram_conn_id=TELEGRAM_CONN_ID,
        chat_id=telegram_chat_id,
    ).send_message(api_params={"text": message, "message_thread_id": telegram_topic_id, "parse_mode": "HTML"})


def _get_proposals(process_id: str, topic_id: str, update_date: str):
    params = {"last_update_time": update_date, "processes_id": process_id}
    df = _get_df_from_sql(PROPOSALS_IN_PROCESSES_SQL, BP_DB, SSH_CONN, params=params)
    df["telegram_topic_id"] = topic_id
    return df.to_dict(orient="records")


default_args = {
    "owner": "Eric Silveira",
    "depends_on_past": False,
    "email_on_failure": True,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=30),
}


@dag(
    default_args=default_args,
    schedule="*/30 * * * *",
    start_date=datetime(2023, 11, 18),
    catchup=False,
    description="DAG for notifying proposals via Telegram",
    max_active_runs=1,
    tags=["notification", "bp", "proposals"],
    is_paused_upon_creation=True,
)
def dag_notify_proposals():
    @task
    def get_telegram_channels():
        channels = _get_df_from_sql(
            """SELECT * FROM "telegram-moderation".telegram_channels
               WHERE telegram_topic_type = 'Propostas' and telegram_topic_id is not null;""",
            CONFIG_DB,
            SSH_CONN,
        )
        if channels.empty:
            logging.warning("No configured channels found.")
            return []
        return channels.to_dict(orient="records")

    @task
    def get_telegram_messages(channels: List[Dict[str, Any]]):
        if not channels:
            logging.info("No channels found for processing Telegram messages.")
            return []

        processes = []
        for channel in channels:
            proposals = _get_proposals(
                channel["participatory_space_id"], channel["telegram_topic_id"], channel["last_message_sent"]
            )
            if proposals:
                processes.extend(proposals)
        return processes

    @task
    def send_telegram(processes: List[Dict[str, Any]]):
        if not processes:
            logging.info("No processes found to send Telegram messages.")
            return []

        for proposal in processes:
            _send_telegram_notification(
                proposal["group_chat_id"],
                proposal["telegram_topic_id"],
                proposal["message_telegram"],
            )

        df = pd.DataFrame.from_records(processes)
        df = df.groupby(by=["group_chat_id", "telegram_topic_id"], as_index=False).aggregate(
            {"updated_at": np.max}
        )
        return df.to_dict("records")

    @task
    def registry_data(messages_log: List[Dict[str, Any]]):
        if not messages_log:
            logging.warning("No messages to log in database.")
            return

        queries = [
            f"""
            UPDATE "telegram-moderation".telegram_channels
            SET last_message_sent = '{message['updated_at']}'
            WHERE telegram_topic_type = 'Propostas'
            AND telegram_group_chat_id = '{message['group_chat_id']}'
            AND telegram_topic_id = {message['telegram_topic_id']};
            """
            for message in messages_log
        ]

        engine = _get_sql_engine(CONFIG_DB, SSH_CONN)
        try:
            with engine.connect() as connection, connection.begin():
                for query in queries:
                    logging.info("Executing query: %s", query)
                    connection.execute(query)
        finally:
            engine.dispose()

    telegram_channels = get_telegram_channels()
    telegram_messages = get_telegram_messages(telegram_channels)
    sent_messages_log = send_telegram(telegram_messages)
    registry_data(sent_messages_log)


dag_notify_proposals()
