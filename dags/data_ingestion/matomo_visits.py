import json
from datetime import datetime, timedelta
from typing import List, Tuple

import pandas as pd
import requests
from airflow import macros
from airflow.datasets import Dataset
from airflow.decorators import dag, task
from airflow.hooks.base_hook import BaseHook
from airflow.operators.empty import EmptyOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook
from sqlalchemy import MetaData, Table
from sqlalchemy.exc import NoSuchTableError
from sqlalchemy.orm import sessionmaker

from plugins.telegram.callbacks import send_telegram

default_args = {"owner": "data", "retries": 5, "retry_delay": timedelta(minutes=10)}

limit = 100


POSTGRES_CONN_ID = "pg_bp_analytics"
SCHEMA = "raw"
TABLE_NAME = "matomo_detailed_visits"
dataset = Dataset("bronze_matomo_detailed_visits")


def _fetch_visits_details(limit=100, **context) -> List[dict]:
    """
    Busca detalhes das visitas utilizando a API Matomo Live! dentro de um intervalo de datas especificado.

    A função consulta a API Matomo, recuperando os
    detalhes das visitas realizadas em um intervalo de datas
    definido pelo contexto de execução do Airflow.
    A busca é realizada de forma paginada até que todos os dados
    sejam obtidos.

    Parâmetros
    ----------
    limit : int, opcional
        Número de registros a serem buscados por página (o padrão é 100).
    **context : dict
        Contexto do Airflow, que inclui informações como a data de início da execução ('ds').

    Retorna
    -------
    list[dict]
        Uma lista contendo os detalhes das visitas no formato JSON. Cada item da lista é um dicionário com
        informações da visita, como:
        - "idVisit": ID único da visita.
        - "actionDetails": Detalhes das ações realizadas durante a visita.
        - "visitLength": Duração da visita em segundos.
        - "serverDate": Data no formato 'YYYY-MM-DD' registrada no servidor.
        - Outros campos conforme a estrutura retornada pela API Matomo.

    Levanta
    -------
    requests.exceptions.HTTPError
        Caso ocorra um erro ao realizar a requisição à API Matomo.
    """
    site_id, matomo_url, api_token = _get_matomo_credentials()
    start_date = context["ds"]
    end_date = macros.ds_add(context["ds"], 1)

    all_visits = []
    offset = 0

    while True:
        params = {
            "module": "API",
            "method": "Live.getLastVisitsDetails",
            "idSite": site_id,
            "period": "range",  # Fetch data within the date range
            "date": f"{start_date},{end_date}",
            "format": "JSON",
            "filter_limit": limit,
            "filter_offset": offset,
            "token_auth": api_token,
        }

        response = requests.get(matomo_url, params=params)
        response.raise_for_status()
        visits = response.json()
        if not visits:
            break
        all_visits.extend(visits)
        offset += limit

    return all_visits


def _get_matomo_credentials() -> Tuple[str, str, str]:
    """
    Retorna as credenciais necessárias para se conectar ao Matomo.

    Returns
    -------
    tuple[str, str, str]
        Uma tupla contendo:
            - site_id (str): O ID do site no Matomo.
            - matomo_url (str): A URL do Matomo.
            - api_token (str): O token de API do Matomo.
    """
    matomo_conn = BaseHook.get_connection("matomo_conn")
    matomo_url = matomo_conn.host
    api_token = matomo_conn.password
    site_id = matomo_conn.login
    return site_id, matomo_url, api_token


def _write_data(data, table=TABLE_NAME, schema=SCHEMA):
    if not data:
        print("No data to write!")
        return None

    engine = PostgresHook(postgres_conn_id=POSTGRES_CONN_ID).get_sqlalchemy_engine()

    df = pd.DataFrame(data)
    pd.set_option("display.max_columns", None)

    def treat_complex_columns(col_value):
        if isinstance(col_value, (dict, list)):
            return json.dumps(col_value, ensure_ascii=False)
        return col_value

    for column in df.columns:
        df[column] = df[column].map(treat_complex_columns)
    try:
        delete_matching_rows_from_table(table, schema, engine, df)
    except NoSuchTableError:
        print(f"Table {schema}.{table} não existe, criando uma nova tabela.")
    df.to_sql(TABLE_NAME, con=engine, schema=SCHEMA, if_exists="append", index=False)
    print(f"DataFrame written to {schema}.{table}.")


def delete_matching_rows_from_table(table, schema, engine, df):
    """Deleta as linhas da tabela especificada com base nos valores únicos da coluna 'serverDate'."""
    sess = sessionmaker(bind=engine)
    metadata = MetaData(schema=schema)
    session = None  # Inicializa a variável session

    try:
        table = Table(table, metadata, autoload_with=engine)
        to_delete = [str(x) for x in df["serverDate"].unique()]
        session = sess()
        delete_query = table.delete().where(table.c.serverDate.in_(to_delete))
        result = session.execute(delete_query)
        session.commit()
        print(f"Rows deleted: {result.rowcount}")
    finally:
        if session:
            session.close()


@dag(
    dag_id="matomo_detailed_visits_ingestion",
    default_args=default_args,
    schedule_interval="0 4 * * *",
    start_date=datetime(2023, 6, 1),
    tags=["ingestion"],
    catchup=True,
    concurrency=1,
    max_active_runs=1,
    render_template_as_native_obj=True,
    on_success_callback=None,
    on_failure_callback=send_telegram,
)
def data_ingestion_matomo_detailed_visits():
    start = EmptyOperator(task_id="start")
    end = EmptyOperator(task_id="end")

    @task(provide_context=True)
    def fetch_visits_details(limit=100, **context):
        return _fetch_visits_details(limit, **context)

    @task(provide_context=True, outlets=dataset)
    def write_data(data, table=TABLE_NAME, schema=SCHEMA):
        _write_data(data, table, schema)

    extract_data_task = fetch_visits_details(limit)
    write_data_task = write_data(extract_data_task, table=TABLE_NAME, schema=SCHEMA)
    start >> extract_data_task >> write_data_task >> end


data_ingestion_matomo_detailed_visits()
