import json
import logging
import os
from datetime import datetime, timedelta
from pathlib import Path

import pandas as pd
from airflow.datasets import Dataset
from airflow.decorators import dag, task
from airflow.operators.empty import EmptyOperator

from plugins.telegram.callbacks import send_telegram

default_args = {
    "owner": "data",
    "retries": 2,
    "retry_delay": timedelta(seconds=20),
    "on_failure_callback": send_telegram,
}

origin_schema = "public"
destination_schema = "raw"


def _extract_data(extraction, extraction_info, db_conn_id, ssh_tunnel: bool = False):
    """
    Extrai dados de uma tabela no banco de dados com suporte opcional a túnel SSH.

    Args:
    ----
        extraction (str): Nome da tabela a ser extraída.
        extraction_info (dict): Informações adicionais sobre a extração, como filtros e transformações.
        db_conn_id (str): ID da conexão com o banco configurada no Airflow.
        ssh_tunnel (bool, optional): Flag para ativar/desativar o túnel SSH. Default é False.

    Returns:
    -------
        pd.DataFrame: DataFrame contendo os dados extraídos da tabela especificada.

    Raises:
    ------
        ValueError: Se transformações SQL contiverem palavras-chave proibidas como 'FROM', 'JOIN', etc.

    Example:
    -------
        >>> extraction_info = {
        ...     "extraction_schema": "public",
        ...     "exclude_columns": ["password"],
        ...     "ingestion_type": "incremental",
        ...     "incremental_filter": "updated_at > '2023-01-01'",
        ... }
        >>> df = _extract_data("users", extraction_info, "my_db_conn", ssh_tunnel=True)
        >>> print(df.head())
    """
    import re

    import pandas as pd
    from airflow.providers.postgres.hooks.postgres import PostgresHook
    from sqlalchemy import MetaData, Table

    extraction_schema = extraction_info["extraction_schema"]
    exclude_columns = set([x.lower() for x in extraction_info.get("exclude_columns", [])])

    engine = PostgresHook(db_conn_id).get_sqlalchemy_engine()

    # Obter todas as colunas da tabela
    try:
        metadata = MetaData(schema=extraction_schema)
        table = Table(extraction, metadata, autoload_with=engine)
        all_columns = set([col.name.lower() for col in table.columns])
        columns = all_columns - exclude_columns

        columns_part = "*"
        where_clause = ""

        if extraction_info.get("sql_transformations"):
            sql_query_part = extraction_info["sql_transformations"]

            where_match = re.search(r"\\bWHERE\\b", sql_query_part, re.IGNORECASE)
            if where_match:
                columns_part = sql_query_part[: where_match.start()].strip()
                where_clause = sql_query_part[where_match.start() :].strip()
            else:
                columns_part = sql_query_part.strip()

            disallowed_keywords = [
                "FROM",
                "JOIN",
                "INSERT",
                "UPDATE",
                "DELETE",
                "DROP",
                "ALTER",
                "GROUP BY",
            ]
            pattern = re.compile(rf"\b({'|'.join(disallowed_keywords)})\b", re.IGNORECASE)
            if pattern.search(columns_part):
                raise ValueError(
                    "O 'sql_transformations' não deve conter cláusulas 'FROM',"
                    "'JOIN' ou outras operações não permitidas."
                )

            selected_columns = set()
            for col_expr in columns_part.split(","):
                col_name = col_expr.strip().split()[-1]
                col_name = col_name.split(".")[-1]
                selected_columns.add(col_name.lower())

            remaining_columns = columns - selected_columns
            final_columns_list = columns_part
            if remaining_columns:
                final_columns_list += ", " + ", ".join(remaining_columns)

        else:
            final_columns_list = ", ".join(columns)

        if extraction_info["ingestion_type"] == "incremental":
            incremental_filter = extraction_info["incremental_filter"]
            if where_clause:
                where_clause += f" AND {incremental_filter}"
            else:
                where_clause = f"WHERE {incremental_filter}"

        final_columns_list = ",".join(
            [f'"{current_column.strip()}"' for current_column in final_columns_list.split(",")]
        )

        query = f"SELECT {final_columns_list} FROM {extraction_schema}.{extraction} {where_clause}"
        df = pd.read_sql(query, engine)

        print(f"Executed {query}")

        if "python_transformations" in extraction_info:
            #! TODO: Ver se tem uma forma melhor de fazer os imports para as transformações do python
            global hashlib
            import hashlib  # noqa: RUF100, F401, I001

            transformations = extraction_info["python_transformations"]
            for transform in transformations:
                column = transform["column"]
                func = transform["function"]
                df[column] = df[column].apply(lambda x, func=func: eval(func, globals(), {"x": x}))

    except Exception as e:
        raise e

    return df


def _verify_columns(
    extraction,
    extraction_info,
    db_origin_conn_id,
    db_dest_conn_id,
    ssh_tunnel: bool = False,
):
    """
    Verifica e sincroniza as colunas entre tabelas de origem e destino.

    Se houver colunas ausentes na tabela de destino, elas serão adicionadas dinamicamente.

    Args:
    ----
        extraction (str): Nome da tabela que será verificada.
        extraction_info (dict): Informções sobre a extração, como schemas de origem e destino.
        db_origin_conn_id (str): ID da conexão do banco de origem no Airflow.
        db_dest_conn_id (str): ID da conexão do banco de destino no Airflow.
        ssh_tunnel (bool, optional): Flag para ativar/desativar o túnel SSH. Default é False.

    Example:
    -------
        >>> _verify_columns("users", extraction_info, "source_conn", "dest_conn", ssh_tunnel=True)
    """
    from airflow.providers.postgres.hooks.postgres import PostgresHook
    from sqlalchemy import MetaData, Table
    from sqlalchemy.exc import NoSuchTableError, ProgrammingError

    engine_origin = PostgresHook(db_origin_conn_id).get_sqlalchemy_engine()
    dest_engine = PostgresHook(db_dest_conn_id).get_sqlalchemy_engine()

    origin_metadata = MetaData(bind=engine_origin, schema=extraction_info["extraction_schema"])
    dest_metadata = MetaData(bind=dest_engine, schema=extraction_info["destination_schema"])

    origin_table = Table(extraction, origin_metadata, autoload_with=engine_origin)

    try:
        dest_table = Table(extraction, dest_metadata, autoload_with=dest_engine)

    except NoSuchTableError:
        print("Table does not exists.")
        return

    origin_table_columns = {col.name: col for col in origin_table.columns}
    dest_table_columns = {col.name: col for col in dest_table.columns}

    missing_columns = {
        name: col for name, col in origin_table_columns.items() if name not in dest_table_columns
    }

    with dest_engine.connect() as conn:
        if "index" in extraction_info:
            index_columns = extraction_info["index"]
            conn.execute(
                f"CREATE INDEX IF NOT EXISTS idx_{extraction}_{'_'.join(index_columns)} on {extraction_info['destination_schema']}.{extraction} ({','.join(index_columns)})"  # noqa: E501
            )

        for name, col in missing_columns.items():
            alter_table_query = (
                f"ALTER TABLE {extraction_info['destination_schema']}.{extraction} "
                f"ADD COLUMN {name} {col.type}"
            )

            try:
                conn.execute(alter_table_query)
                print(f"Columns added: {name} type: {col.type}")

            except ProgrammingError as e:
                print(f"Failed to add column '{name}' in {conn.engine.url}: {e}")


def _write_data(df: pd.DataFrame, extraction: str, extraction_info: dict, db_conn_id: str):
    """
    Escreve os dados extraídos em uma tabela de destino.

    Aplica tratamento para colunas complexas e realiza deleções necessárias antes de inserir os dados.

    Args:
    ----
        df (pd.DataFrame): DataFrame contendo os dados a serem escritos.
        extraction (str): Nome da tabela de destino.
        extraction_info (dict): Informações sobre a extração e tipo de ingestão.
        db_conn_id (str): ID da conexão do banco de destino no Airflow.

    Example:
    -------
        >>> _write_data(df, "users", extraction_info, "dest_conn")
    """
    import json

    import numpy as np
    from airflow.providers.postgres.hooks.postgres import PostgresHook
    from sqlalchemy import MetaData, Table
    from sqlalchemy.exc import NoSuchTableError
    from sqlalchemy.sql import any_
    from sqlalchemy.orm import sessionmaker


    def treat_complex_columns(col_value):
        if isinstance(col_value, (dict, list)):
            return json.dumps(col_value, ensure_ascii=False)
        return col_value

    if df.empty:
        print("No data to write!")
        return None

    engine = PostgresHook(db_conn_id).get_sqlalchemy_engine()

    df = df.applymap(treat_complex_columns)

    schema = extraction_info["destination_schema"]
    insertion_method = "append"

    sess = sessionmaker(bind=engine)
    session = sess()
    metadata = MetaData(schema=schema)
    try:
        table = Table(extraction, metadata, autoload_with=engine)

        # Delete existing records based on ingestion type
        if extraction_info.get("ingestion_type") == "incremental":
            unique_ids = df["id"].unique()

            # Process batches with configurable batch size
            batch_size = extraction_info.get("batch_size", 50_000)
            batches = np.array_split(unique_ids, batch_size)

            total_deleted = 0
            for ids_batch in batches:
                if len(ids_batch) == 0:
                    continue

                # Convert IDs to appropriate type and handle potential type issues
                try:
                    typed_ids = [int(x) for x in ids_batch]
                except (ValueError, TypeError):
                    typed_ids = [str(x) for x in ids_batch]

                delete_query = table.delete().where(table.c.id == any_(typed_ids))
                result = session.execute(delete_query)
                total_deleted += result.rowcount

                # Commit after each batch to avoid long transactions
                session.commit()
        else:
            # Full refresh - delete all records
            delete_query = table.delete()
            result = session.execute(delete_query)
            total_deleted = result.rowcount
            session.commit()

        # Log deletion results
        logging.info("Rows deleted from %s.%s: %s", schema, extraction, total_deleted)

    except NoSuchTableError:
        print("Table does not exists.")

    session.close()

    # Insert new data
    if not df.empty:
        df.to_sql(
            name=extraction,
            con=engine,
            schema=schema,
            if_exists=insertion_method,
            index=False,
            chunksize=10000,
        )
        logging.info("Inserted %s rows into %s.%s", len(df), schema, extraction)
    else:
        logging.warning("No data to insert into %s.%s", schema, extraction)


for entry in os.scandir(Path(__file__).parent.joinpath("./cursor_ingestions")):
    if not entry.name.endswith(".json"):
        continue

    with open(entry.path) as file:
        dag_config = json.load(file)

    dag_name: str = dag_config["name"]
    extractions: dict = dag_config["extractions"]
    catchup: bool = dag_config["catchup"]
    start_date: str = datetime.strptime(dag_config["start_date"], "%Y-%m-%d")
    schedule_interval: str = dag_config.get("schedule_interval", "0 4 * * *")

    @dag(
        dag_id=f"{dag_name}_postgres_cursor_ingestion",
        default_args=default_args,
        schedule_interval=schedule_interval,
        start_date=start_date,
        tags=["ingestion", dag_name],
        catchup=catchup,
        concurrency=5,
        max_active_runs=1,
        render_template_as_native_obj=True,
    )
    def data_ingestion_postgres(origin_db_connection, destination_db_connection):
        start = EmptyOperator(task_id="start")
        end = EmptyOperator(task_id="end")

        for extraction, extraction_info in extractions.items():  # noqa: B023

            @task(task_id=f"extract_data_{extraction}")
            def extract_data(
                extraction, extraction_info, db_conn_id, ssh_tunnel: bool = False
            ) -> pd.DataFrame:
                return _extract_data(extraction, extraction_info, db_conn_id, ssh_tunnel)

            @task(task_id=f"verify_columns_{extraction}")
            def verify_columns(
                extraction,
                extraction_info,
                db_origin_conn_id,
                db_dest_conn_id,
                ssh_tunnel: bool = False,
            ):
                _verify_columns(
                    extraction,
                    extraction_info,
                    db_origin_conn_id,
                    db_dest_conn_id,
                    ssh_tunnel,
                )

            @task(
                task_id=f"write_data_{extraction}",
                outlets=[Dataset(f"bronze_{extraction}")],
            )
            def write_data(df, extraction, extraction_info, db_conn_id):
                _write_data(df, extraction, extraction_info, db_conn_id)

            extract_data_task = extract_data(extraction, extraction_info, origin_db_connection, True)

            verify_columns_task = verify_columns(
                extraction,
                extraction_info,
                origin_db_connection,
                destination_db_connection,
                True,
            )

            write_data_task = write_data(
                extract_data_task,
                extraction,
                extraction_info,
                destination_db_connection,
            )

            start >> extract_data_task >> verify_columns_task >> write_data_task >> end

    data_ingestion_postgres(dag_config["origin_db_connection"], dag_config["destination_db_connection"])
