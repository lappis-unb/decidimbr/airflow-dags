# fmt: off
# ruff: noqa

from airflow import DAG
from airflow.datasets import Dataset
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.utils.dates import days_ago
from plugins.telegram.callbacks import send_telegram

from datetime import timedelta

default_args = {
    'owner': 'DBT-Genrated',
    'retries': 5,
    'retry_delay': timedelta(minutes=10),
    'pool': 'dbt_pool'
}

with DAG(
    "run_model__dates",
    default_args=default_args,
    schedule=[Dataset('bronze_dates')],
    start_date=days_ago(1),
    tags=["dbt", "model"],
    max_active_runs=1,
    on_success_callback=None,
    on_failure_callback=send_telegram,
) as dag:

    end_task = EmptyOperator(
        task_id="end",
        outlets=[Dataset("dates_model")],
    )

    dates_task = BashOperator(
        task_id='run_dates',
        bash_command='rm -r /tmp/dbt_run_dates || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_run_dates \
&& cd /tmp/dbt_run_dates \
&& dbt deps && dbt run --select dates \
&& rm -r /tmp/dbt_run_dates',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_dates_year_month_day_task = BashOperator(
        task_id='test_not_null_dates_year_month_day',
        bash_command='rm -r /tmp/dbt_test_not_null_dates_year_month_day || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_dates_year_month_day \
&& cd /tmp/dbt_test_not_null_dates_year_month_day \
&& dbt deps && dbt test --select not_null_dates_year_month_day \
&& rm -r /tmp/dbt_test_not_null_dates_year_month_day',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    unique_dates_year_month_day_task = BashOperator(
        task_id='test_unique_dates_year_month_day',
        bash_command='rm -r /tmp/dbt_test_unique_dates_year_month_day || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_unique_dates_year_month_day \
&& cd /tmp/dbt_test_unique_dates_year_month_day \
&& dbt deps && dbt test --select unique_dates_year_month_day \
&& rm -r /tmp/dbt_test_unique_dates_year_month_day',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_dates_year_month_task = BashOperator(
        task_id='test_not_null_dates_year_month',
        bash_command='rm -r /tmp/dbt_test_not_null_dates_year_month || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_dates_year_month \
&& cd /tmp/dbt_test_not_null_dates_year_month \
&& dbt deps && dbt test --select not_null_dates_year_month \
&& rm -r /tmp/dbt_test_not_null_dates_year_month',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_dates_year_task = BashOperator(
        task_id='test_not_null_dates_year',
        bash_command='rm -r /tmp/dbt_test_not_null_dates_year || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_dates_year \
&& cd /tmp/dbt_test_not_null_dates_year \
&& dbt deps && dbt test --select not_null_dates_year \
&& rm -r /tmp/dbt_test_not_null_dates_year',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_dates_week_day_description_task = BashOperator(
        task_id='test_not_null_dates_week_day_description',
        bash_command='rm -r /tmp/dbt_test_not_null_dates_week_day_description || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_dates_week_day_description \
&& cd /tmp/dbt_test_not_null_dates_week_day_description \
&& dbt deps && dbt test --select not_null_dates_week_day_description \
&& rm -r /tmp/dbt_test_not_null_dates_week_day_description',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_dates_month_description_task = BashOperator(
        task_id='test_not_null_dates_month_description',
        bash_command='rm -r /tmp/dbt_test_not_null_dates_month_description || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_dates_month_description \
&& cd /tmp/dbt_test_not_null_dates_month_description \
&& dbt deps && dbt test --select not_null_dates_month_description \
&& rm -r /tmp/dbt_test_not_null_dates_month_description',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_dates_month_number_task = BashOperator(
        task_id='test_not_null_dates_month_number',
        bash_command='rm -r /tmp/dbt_test_not_null_dates_month_number || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_dates_month_number \
&& cd /tmp/dbt_test_not_null_dates_month_number \
&& dbt deps && dbt test --select not_null_dates_month_number \
&& rm -r /tmp/dbt_test_not_null_dates_month_number',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_dates_day_of_month_task = BashOperator(
        task_id='test_not_null_dates_day_of_month',
        bash_command='rm -r /tmp/dbt_test_not_null_dates_day_of_month || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_dates_day_of_month \
&& cd /tmp/dbt_test_not_null_dates_day_of_month \
&& dbt deps && dbt test --select not_null_dates_day_of_month \
&& rm -r /tmp/dbt_test_not_null_dates_day_of_month',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_dates_day_of_week_task = BashOperator(
        task_id='test_not_null_dates_day_of_week',
        bash_command='rm -r /tmp/dbt_test_not_null_dates_day_of_week || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_dates_day_of_week \
&& cd /tmp/dbt_test_not_null_dates_day_of_week \
&& dbt deps && dbt test --select not_null_dates_day_of_week \
&& rm -r /tmp/dbt_test_not_null_dates_day_of_week',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_dates_day_of_year_task = BashOperator(
        task_id='test_not_null_dates_day_of_year',
        bash_command='rm -r /tmp/dbt_test_not_null_dates_day_of_year || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_dates_day_of_year \
&& cd /tmp/dbt_test_not_null_dates_day_of_year \
&& dbt deps && dbt test --select not_null_dates_day_of_year \
&& rm -r /tmp/dbt_test_not_null_dates_day_of_year',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_dates_week_of_month_task = BashOperator(
        task_id='test_not_null_dates_week_of_month',
        bash_command='rm -r /tmp/dbt_test_not_null_dates_week_of_month || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_dates_week_of_month \
&& cd /tmp/dbt_test_not_null_dates_week_of_month \
&& dbt deps && dbt test --select not_null_dates_week_of_month \
&& rm -r /tmp/dbt_test_not_null_dates_week_of_month',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_dates_weekend_task = BashOperator(
        task_id='test_not_null_dates_weekend',
        bash_command='rm -r /tmp/dbt_test_not_null_dates_weekend || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_dates_weekend \
&& cd /tmp/dbt_test_not_null_dates_weekend \
&& dbt deps && dbt test --select not_null_dates_weekend \
&& rm -r /tmp/dbt_test_not_null_dates_weekend',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_dates_week_of_year_task = BashOperator(
        task_id='test_not_null_dates_week_of_year',
        bash_command='rm -r /tmp/dbt_test_not_null_dates_week_of_year || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_dates_week_of_year \
&& cd /tmp/dbt_test_not_null_dates_week_of_year \
&& dbt deps && dbt test --select not_null_dates_week_of_year \
&& rm -r /tmp/dbt_test_not_null_dates_week_of_year',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_dates_holiday_flag_task = BashOperator(
        task_id='test_not_null_dates_holiday_flag',
        bash_command='rm -r /tmp/dbt_test_not_null_dates_holiday_flag || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_dates_holiday_flag \
&& cd /tmp/dbt_test_not_null_dates_holiday_flag \
&& dbt deps && dbt test --select not_null_dates_holiday_flag \
&& rm -r /tmp/dbt_test_not_null_dates_holiday_flag',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    dates_task >> not_null_dates_year_month_day_task >> end_task
    dates_task >> unique_dates_year_month_day_task >> end_task
    dates_task >> not_null_dates_year_month_task >> end_task
    dates_task >> not_null_dates_year_task >> end_task
    dates_task >> not_null_dates_week_day_description_task >> end_task
    dates_task >> not_null_dates_month_description_task >> end_task
    dates_task >> not_null_dates_month_number_task >> end_task
    dates_task >> not_null_dates_day_of_month_task >> end_task
    dates_task >> not_null_dates_day_of_week_task >> end_task
    dates_task >> not_null_dates_day_of_year_task >> end_task
    dates_task >> not_null_dates_week_of_month_task >> end_task
    dates_task >> not_null_dates_weekend_task >> end_task
    dates_task >> not_null_dates_week_of_year_task >> end_task
    dates_task >> not_null_dates_holiday_flag_task >> end_task
