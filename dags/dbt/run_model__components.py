# fmt: off
# ruff: noqa

from airflow import DAG
from airflow.datasets import Dataset
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.utils.dates import days_ago
from plugins.telegram.callbacks import send_telegram

from datetime import timedelta

default_args = {
    'owner': 'DBT-Genrated',
    'retries': 5,
    'retry_delay': timedelta(minutes=10),
    'pool': 'dbt_pool'
}

with DAG(
    "run_model__components",
    default_args=default_args,
    schedule=[Dataset('bronze_decidim_components'), Dataset('bronze_decidim_participatory_processes'), Dataset('bronze_decidim_participatory_process_types'), Dataset('bronze_decidim_areas'), Dataset('bronze_decidim_categories')],
    start_date=days_ago(1),
    tags=["dbt", "model"],
    max_active_runs=1,
    on_success_callback=None,
    on_failure_callback=send_telegram,
) as dag:

    end_task = EmptyOperator(
        task_id="end",
        outlets=[Dataset("components_model")],
    )

    components_task = BashOperator(
        task_id='run_components',
        bash_command='rm -r /tmp/dbt_run_components || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_run_components \
&& cd /tmp/dbt_run_components \
&& dbt deps && dbt run --select components \
&& rm -r /tmp/dbt_run_components',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    unique_components_component_id_task = BashOperator(
        task_id='test_unique_components_component_id',
        bash_command='rm -r /tmp/dbt_test_unique_components_component_id || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_unique_components_component_id \
&& cd /tmp/dbt_test_unique_components_component_id \
&& dbt deps && dbt test --select unique_components_component_id \
&& rm -r /tmp/dbt_test_unique_components_component_id',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_components_component_id_task = BashOperator(
        task_id='test_not_null_components_component_id',
        bash_command='rm -r /tmp/dbt_test_not_null_components_component_id || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_components_component_id \
&& cd /tmp/dbt_test_not_null_components_component_id \
&& dbt deps && dbt test --select not_null_components_component_id \
&& rm -r /tmp/dbt_test_not_null_components_component_id',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    column_completeness_test_source_components_component_id__id__bronze__decidim_components_task = BashOperator(
        task_id='test_column_completeness_test_source_components_component_id__id__bronze__decidim_components',
        bash_command='rm -r /tmp/dbt_test_column_completeness_test_source_components_component_id__id__bronze__decidim_components || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_column_completeness_test_source_components_component_id__id__bronze__decidim_components \
&& cd /tmp/dbt_test_column_completeness_test_source_components_component_id__id__bronze__decidim_components \
&& dbt deps && dbt test --select column_completeness_test_source_components_component_id__id__bronze__decidim_components \
&& rm -r /tmp/dbt_test_column_completeness_test_source_components_component_id__id__bronze__decidim_components',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_components_participatory_space_id_task = BashOperator(
        task_id='test_not_null_components_participatory_space_id',
        bash_command='rm -r /tmp/dbt_test_not_null_components_participatory_space_id || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_components_participatory_space_id \
&& cd /tmp/dbt_test_not_null_components_participatory_space_id \
&& dbt deps && dbt test --select not_null_components_participatory_space_id \
&& rm -r /tmp/dbt_test_not_null_components_participatory_space_id',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    referential_integrity_test_components_participatory_space_id__process_id__participatory_processes_task = BashOperator(
        task_id='test_referential_integrity_test_components_participatory_space_id__process_id__participatory_processes',
        bash_command='rm -r /tmp/dbt_test_referential_integrity_test_components_participatory_space_id__process_id__participatory_processes || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_referential_integrity_test_components_participatory_space_id__process_id__participatory_processes \
&& cd /tmp/dbt_test_referential_integrity_test_components_participatory_space_id__process_id__participatory_processes \
&& dbt deps && dbt test --select referential_integrity_test_components_participatory_space_id__process_id__participatory_processes \
&& rm -r /tmp/dbt_test_referential_integrity_test_components_participatory_space_id__process_id__participatory_processes',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_components_id_task = BashOperator(
        task_id='test_not_null_components_id',
        bash_command='rm -r /tmp/dbt_test_not_null_components_id || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_components_id \
&& cd /tmp/dbt_test_not_null_components_id \
&& dbt deps && dbt test --select not_null_components_id \
&& rm -r /tmp/dbt_test_not_null_components_id',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    referential_integrity_test_components_id__process_id__participatory_processes_task = BashOperator(
        task_id='test_referential_integrity_test_components_id__process_id__participatory_processes',
        bash_command='rm -r /tmp/dbt_test_referential_integrity_test_components_id__process_id__participatory_processes || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_referential_integrity_test_components_id__process_id__participatory_processes \
&& cd /tmp/dbt_test_referential_integrity_test_components_id__process_id__participatory_processes \
&& dbt deps && dbt test --select referential_integrity_test_components_id__process_id__participatory_processes \
&& rm -r /tmp/dbt_test_referential_integrity_test_components_id__process_id__participatory_processes',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    referential_integrity_test_components_decidim_scope_id__id__scopes_task = BashOperator(
        task_id='test_referential_integrity_test_components_decidim_scope_id__id__scopes',
        bash_command='rm -r /tmp/dbt_test_referential_integrity_test_components_decidim_scope_id__id__scopes || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_referential_integrity_test_components_decidim_scope_id__id__scopes \
&& cd /tmp/dbt_test_referential_integrity_test_components_decidim_scope_id__id__scopes \
&& dbt deps && dbt test --select referential_integrity_test_components_decidim_scope_id__id__scopes \
&& rm -r /tmp/dbt_test_referential_integrity_test_components_decidim_scope_id__id__scopes',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    referential_integrity_test_components_decidim_participatory_process_group_id__id__participatory_process_groups_task = BashOperator(
        task_id='test_referential_integrity_test_components_decidim_participatory_process_group_id__id__participatory_process_groups',
        bash_command='rm -r /tmp/dbt_test_referential_integrity_test_components_decidim_participatory_process_group_id__id__participatory_process_groups || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_referential_integrity_test_components_decidim_participatory_process_group_id__id__participatory_process_groups \
&& cd /tmp/dbt_test_referential_integrity_test_components_decidim_participatory_process_group_id__id__participatory_process_groups \
&& dbt deps && dbt test --select referential_integrity_test_components_decidim_participatory_process_group_id__id__participatory_process_groups \
&& rm -r /tmp/dbt_test_referential_integrity_test_components_decidim_participatory_process_group_id__id__participatory_process_groups',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    referential_integrity_test_components_decidim_area_id__da_id__areas_task = BashOperator(
        task_id='test_referential_integrity_test_components_decidim_area_id__da_id__areas',
        bash_command='rm -r /tmp/dbt_test_referential_integrity_test_components_decidim_area_id__da_id__areas || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_referential_integrity_test_components_decidim_area_id__da_id__areas \
&& cd /tmp/dbt_test_referential_integrity_test_components_decidim_area_id__da_id__areas \
&& dbt deps && dbt test --select referential_integrity_test_components_decidim_area_id__da_id__areas \
&& rm -r /tmp/dbt_test_referential_integrity_test_components_decidim_area_id__da_id__areas',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    referential_integrity_test_components_decidim_scope_type_id__id__scope_types_task = BashOperator(
        task_id='test_referential_integrity_test_components_decidim_scope_type_id__id__scope_types',
        bash_command='rm -r /tmp/dbt_test_referential_integrity_test_components_decidim_scope_type_id__id__scope_types || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_referential_integrity_test_components_decidim_scope_type_id__id__scope_types \
&& cd /tmp/dbt_test_referential_integrity_test_components_decidim_scope_type_id__id__scope_types \
&& dbt deps && dbt test --select referential_integrity_test_components_decidim_scope_type_id__id__scope_types \
&& rm -r /tmp/dbt_test_referential_integrity_test_components_decidim_scope_type_id__id__scope_types',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    referential_integrity_test_components_decidim_participatory_process_type_id__dppt_id__participatory_process_types_task = BashOperator(
        task_id='test_referential_integrity_test_components_decidim_participatory_process_type_id__dppt_id__participatory_process_types',
        bash_command='rm -r /tmp/dbt_test_referential_integrity_test_components_decidim_participatory_process_type_id__dppt_id__participatory_process_types || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_referential_integrity_test_components_decidim_participatory_process_type_id__dppt_id__participatory_process_types \
&& cd /tmp/dbt_test_referential_integrity_test_components_decidim_participatory_process_type_id__dppt_id__participatory_process_types \
&& dbt deps && dbt test --select referential_integrity_test_components_decidim_participatory_process_type_id__dppt_id__participatory_process_types \
&& rm -r /tmp/dbt_test_referential_integrity_test_components_decidim_participatory_process_type_id__dppt_id__participatory_process_types',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    referential_integrity_test_components_initial_page_component_id__component_id__components_task = BashOperator(
        task_id='test_referential_integrity_test_components_initial_page_component_id__component_id__components',
        bash_command='rm -r /tmp/dbt_test_referential_integrity_test_components_initial_page_component_id__component_id__components || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_referential_integrity_test_components_initial_page_component_id__component_id__components \
&& cd /tmp/dbt_test_referential_integrity_test_components_initial_page_component_id__component_id__components \
&& dbt deps && dbt test --select referential_integrity_test_components_initial_page_component_id__component_id__components \
&& rm -r /tmp/dbt_test_referential_integrity_test_components_initial_page_component_id__component_id__components',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    components_task >> unique_components_component_id_task >> end_task
    components_task >> not_null_components_component_id_task >> end_task
    components_task >> column_completeness_test_source_components_component_id__id__bronze__decidim_components_task >> end_task
    components_task >> not_null_components_participatory_space_id_task >> end_task
    components_task >> referential_integrity_test_components_participatory_space_id__process_id__participatory_processes_task >> end_task
    components_task >> not_null_components_id_task >> end_task
    components_task >> referential_integrity_test_components_id__process_id__participatory_processes_task >> end_task
    components_task >> referential_integrity_test_components_decidim_scope_id__id__scopes_task >> end_task
    components_task >> referential_integrity_test_components_decidim_participatory_process_group_id__id__participatory_process_groups_task >> end_task
    components_task >> referential_integrity_test_components_decidim_area_id__da_id__areas_task >> end_task
    components_task >> referential_integrity_test_components_decidim_scope_type_id__id__scope_types_task >> end_task
    components_task >> referential_integrity_test_components_decidim_participatory_process_type_id__dppt_id__participatory_process_types_task >> end_task
    components_task >> referential_integrity_test_components_initial_page_component_id__component_id__components_task >> end_task
