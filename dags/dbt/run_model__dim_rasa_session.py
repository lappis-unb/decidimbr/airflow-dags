# fmt: off
# ruff: noqa

from airflow import DAG
from airflow.datasets import Dataset
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.utils.dates import days_ago
from plugins.telegram.callbacks import send_telegram

from datetime import timedelta

default_args = {
    'owner': 'DBT-Genrated',
    'retries': 5,
    'retry_delay': timedelta(minutes=10),
    'pool': 'dbt_pool'
}

with DAG(
    "run_model__dim_rasa_session",
    default_args=default_args,
    schedule='@daily',
    start_date=days_ago(1),
    tags=["dbt", "model"],
    max_active_runs=1,
    on_success_callback=None,
    on_failure_callback=send_telegram,
) as dag:

    end_task = EmptyOperator(
        task_id="end",
        outlets=[Dataset("dim_rasa_session_model")],
    )

    dim_rasa_session_task = BashOperator(
        task_id='run_dim_rasa_session',
        bash_command='rm -r /tmp/dbt_run_dim_rasa_session || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_run_dim_rasa_session \
&& cd /tmp/dbt_run_dim_rasa_session \
&& dbt deps && dbt run --select dim_rasa_session \
&& rm -r /tmp/dbt_run_dim_rasa_session',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    dim_rasa_session_task >> end_task
