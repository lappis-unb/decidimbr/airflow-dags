# fmt: off
# ruff: noqa

from airflow import DAG
from airflow.datasets import Dataset
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.utils.dates import days_ago
from plugins.telegram.callbacks import send_telegram

from datetime import timedelta

default_args = {
    'owner': 'DBT-Genrated',
    'retries': 5,
    'retry_delay': timedelta(minutes=10),
    'pool': 'dbt_pool'
}

with DAG(
    "run_model__ej_votes",
    default_args=default_args,
    schedule=[Dataset('bronze_ej_users_user'), Dataset('bronze_ej_conversations_vote'), Dataset('bronze_ej_conversations_comment'), Dataset('bronze_ej_conversations_conversation')],
    start_date=days_ago(1),
    tags=["dbt", "model"],
    max_active_runs=1,
    on_success_callback=None,
    on_failure_callback=send_telegram,
) as dag:

    end_task = EmptyOperator(
        task_id="end",
        outlets=[Dataset("ej_votes_model")],
    )

    ej_votes_task = BashOperator(
        task_id='run_ej_votes',
        bash_command='rm -r /tmp/dbt_run_ej_votes || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_run_ej_votes \
&& cd /tmp/dbt_run_ej_votes \
&& dbt deps && dbt run --select ej_votes \
&& rm -r /tmp/dbt_run_ej_votes',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_ej_votes_id_usuario_task = BashOperator(
        task_id='test_not_null_ej_votes_id_usuario',
        bash_command='rm -r /tmp/dbt_test_not_null_ej_votes_id_usuario || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_ej_votes_id_usuario \
&& cd /tmp/dbt_test_not_null_ej_votes_id_usuario \
&& dbt deps && dbt test --select not_null_ej_votes_id_usuario \
&& rm -r /tmp/dbt_test_not_null_ej_votes_id_usuario',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_ej_votes_ej_id_enquete_task = BashOperator(
        task_id='test_not_null_ej_votes_ej_id_enquete',
        bash_command='rm -r /tmp/dbt_test_not_null_ej_votes_ej_id_enquete || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_ej_votes_ej_id_enquete \
&& cd /tmp/dbt_test_not_null_ej_votes_ej_id_enquete \
&& dbt deps && dbt test --select not_null_ej_votes_ej_id_enquete \
&& rm -r /tmp/dbt_test_not_null_ej_votes_ej_id_enquete',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_ej_votes_ej_id_pergunta_task = BashOperator(
        task_id='test_not_null_ej_votes_ej_id_pergunta',
        bash_command='rm -r /tmp/dbt_test_not_null_ej_votes_ej_id_pergunta || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_ej_votes_ej_id_pergunta \
&& cd /tmp/dbt_test_not_null_ej_votes_ej_id_pergunta \
&& dbt deps && dbt test --select not_null_ej_votes_ej_id_pergunta \
&& rm -r /tmp/dbt_test_not_null_ej_votes_ej_id_pergunta',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_ej_votes_codigo_escolha_task = BashOperator(
        task_id='test_not_null_ej_votes_codigo_escolha',
        bash_command='rm -r /tmp/dbt_test_not_null_ej_votes_codigo_escolha || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_ej_votes_codigo_escolha \
&& cd /tmp/dbt_test_not_null_ej_votes_codigo_escolha \
&& dbt deps && dbt test --select not_null_ej_votes_codigo_escolha \
&& rm -r /tmp/dbt_test_not_null_ej_votes_codigo_escolha',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_ej_votes_descricao_escolha_task = BashOperator(
        task_id='test_not_null_ej_votes_descricao_escolha',
        bash_command='rm -r /tmp/dbt_test_not_null_ej_votes_descricao_escolha || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_ej_votes_descricao_escolha \
&& cd /tmp/dbt_test_not_null_ej_votes_descricao_escolha \
&& dbt deps && dbt test --select not_null_ej_votes_descricao_escolha \
&& rm -r /tmp/dbt_test_not_null_ej_votes_descricao_escolha',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_ej_votes_pergunta_enquete_task = BashOperator(
        task_id='test_not_null_ej_votes_pergunta_enquete',
        bash_command='rm -r /tmp/dbt_test_not_null_ej_votes_pergunta_enquete || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_ej_votes_pergunta_enquete \
&& cd /tmp/dbt_test_not_null_ej_votes_pergunta_enquete \
&& dbt deps && dbt test --select not_null_ej_votes_pergunta_enquete \
&& rm -r /tmp/dbt_test_not_null_ej_votes_pergunta_enquete',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_ej_votes_canal_voto_task = BashOperator(
        task_id='test_not_null_ej_votes_canal_voto',
        bash_command='rm -r /tmp/dbt_test_not_null_ej_votes_canal_voto || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_ej_votes_canal_voto \
&& cd /tmp/dbt_test_not_null_ej_votes_canal_voto \
&& dbt deps && dbt test --select not_null_ej_votes_canal_voto \
&& rm -r /tmp/dbt_test_not_null_ej_votes_canal_voto',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    not_null_ej_votes_data_voto_task = BashOperator(
        task_id='test_not_null_ej_votes_data_voto',
        bash_command='rm -r /tmp/dbt_test_not_null_ej_votes_data_voto || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt_pg_project /tmp/dbt_test_not_null_ej_votes_data_voto \
&& cd /tmp/dbt_test_not_null_ej_votes_data_voto \
&& dbt deps && dbt test --select not_null_ej_votes_data_voto \
&& rm -r /tmp/dbt_test_not_null_ej_votes_data_voto',
        env={
            'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
        },
        append_env=True
    )

    ej_votes_task >> not_null_ej_votes_id_usuario_task >> end_task
    ej_votes_task >> not_null_ej_votes_ej_id_enquete_task >> end_task
    ej_votes_task >> not_null_ej_votes_ej_id_pergunta_task >> end_task
    ej_votes_task >> not_null_ej_votes_codigo_escolha_task >> end_task
    ej_votes_task >> not_null_ej_votes_descricao_escolha_task >> end_task
    ej_votes_task >> not_null_ej_votes_pergunta_enquete_task >> end_task
    ej_votes_task >> not_null_ej_votes_canal_voto_task >> end_task
    ej_votes_task >> not_null_ej_votes_data_voto_task >> end_task
