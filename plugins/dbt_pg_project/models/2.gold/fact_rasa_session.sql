{{ config(
    materialized="incremental",
    schema="gold",
    unique_key="session"
) }}

{% set metadata_key = "'metadata'" %}

WITH sessions AS (
    SELECT
        sk AS session,
        user_id,
        DATE(start_timestamp) AS data_started,
        start_timestamp,
        end_timestamp AS end_session_timestamp,
        (end_timestamp - start_timestamp) AS session_duration
    FROM {{ ref('dim_rasa_session') }}
),

last_processed_session AS (
    SELECT
        {% if is_incremental() %}
            COALESCE(MAX(session), 0) AS last_session
        FROM {{ this }}
        {% else %}
            0 AS last_session
        {% endif %}
),

filtered_sessions AS (
    SELECT
        *
    FROM sessions
    WHERE session > (SELECT last_session FROM last_processed_session)
),

bot_messages AS (
    SELECT
        session,
        COUNT(sk) AS num_bot_messages,
        COUNT(*) FILTER (
            WHERE data::json->{{ metadata_key }}->>'utter_action' = 'utter_invalid_vote_during_participation'
        ) AS num_invalid_votes,
        COUNT(*) FILTER (
            WHERE data::json->{{ metadata_key }}->>'utter_action' = 'utter_ej_communication_error'
        ) AS num_communication_errors
    FROM {{ ref('dim_rasa_bot') }}
    GROUP BY session
),

user_messages AS (
    SELECT
        session,
        COUNT(sk) AS num_user_messages,
        COUNT(*) FILTER (
            WHERE intent_name = 'help'
        ) AS num_help_request
    FROM {{ ref('dim_rasa_user') }}
    GROUP BY session
),

active_loops AS (
    SELECT
        session,
        COUNT(sk) AS num_active_loops,
        GREATEST(COUNT(*) FILTER (
            WHERE form_name = 'vote_form'
        ) - 1, 0) AS num_interrupted_flows,
        COUNT(*) FILTER (
            WHERE form_name = 'authentication_form'
        ) AS num_authentication_attempts
    FROM {{ ref('dim_rasa_active_loop') }}
    GROUP BY session
),

slots AS (
    SELECT
        session,
        COUNT(sk) AS num_slots,
        COUNT(*) FILTER (
            WHERE name = 'vote'
        ) AS num_votes,
        COUNT(*) FILTER (
            WHERE name = 'votes_to_send_profile_questions'
        ) AS num_profile_questions_sent,
        COUNT(*) FILTER (
            WHERE name = 'comment_content'
        ) AS num_comments_sent
    FROM {{ ref('dim_rasa_slot') }}
    GROUP BY session
),

actions AS (
    SELECT
        session,
        COUNT(sk) AS num_actions,
        AVG(confidence) AS avg_bot_confidence,
        COUNT(*) FILTER (
            WHERE action_name = 'utter_stoped_participation'
        ) AS num_participation_stops,
        COUNT(*) FILTER (
            WHERE action_name = 'action_restart'
        ) AS num_session_restarts
    FROM {{ ref('dim_rasa_action') }}
    GROUP BY session
),

participation_ratios AS (
    SELECT
        session,
        MAX((value::jsonb)->>'participation_ratio')::FLOAT AS participation_percentage
    FROM {{ ref('dim_rasa_slot') }}
    WHERE name = 'user_statistics'
    GROUP BY session
),

idle_times AS (
    SELECT
        se.session,
        se.event_type,
        se.datetime AS timestamp,
        LEAD(se.datetime) OVER (
            PARTITION BY se.session
            ORDER BY se.datetime
        ) AS next_timestamp,
        COALESCE(
            LEAD(se.datetime) OVER (PARTITION BY se.session ORDER BY se.datetime) - se.datetime,
            '0 seconds'::interval
        ) AS idle_time
    FROM (
        SELECT
            session,
            'bot' AS event_type,
            datetime
        FROM {{ ref('dim_rasa_bot') }}
        UNION ALL
        SELECT
            session,
            'user' AS event_type,
            datetime
        FROM {{ ref('dim_rasa_user') }}
    ) se
),

max_idle_times AS (
    SELECT
        session,
        MAX(idle_time) AS max_idle_time
    FROM idle_times
    WHERE event_type = 'user'
    GROUP BY session
),

session_starts AS (
    SELECT
        s.sk AS session,
        e.id AS start_event_id,
        e.data::json->{{ metadata_key }}->>'model_id' AS model_id
    FROM {{ ref('dim_rasa_session') }} s
    JOIN {{ source('bronze', 'events') }} e
    ON s.start = e.id
    WHERE e.data::json->{{ metadata_key }}->>'model_id' IS NOT NULL
),

model_associations AS (
    SELECT
        ss.session,
        dm.sk AS model_sk
    FROM session_starts ss
    JOIN {{ ref('dim_rasa_model') }} dm
    ON ss.model_id = dm.model_id
)

SELECT
    fs.session,
    fs.user_id,
    ma.model_sk AS model_id,
    fs.data_started,
    fs.start_timestamp,
    fs.end_session_timestamp,
    fs.session_duration,

    COALESCE(bm.num_bot_messages, 0) AS num_bot_messages,
    COALESCE(bm.num_invalid_votes, 0) AS num_invalid_votes,
    COALESCE(bm.num_communication_errors, 0) AS num_communication_errors,

    COALESCE(um.num_user_messages, 0) AS num_user_messages,
    COALESCE(um.num_help_request, 0) AS num_help_request,

    COALESCE(al.num_active_loops, 0) AS num_active_loops,
    COALESCE(al.num_interrupted_flows, 0) AS num_interrupted_flows,
    COALESCE(al.num_authentication_attempts, 0) AS num_authentication_attempts,

    COALESCE(sl.num_slots, 0) AS num_slots,
    COALESCE(sl.num_votes, 0) AS num_votes,
    COALESCE(sl.num_profile_questions_sent, 0) AS num_profile_questions_sent,
    COALESCE(sl.num_comments_sent, 0) AS num_comments_sent,

    COALESCE(a.num_actions, 0) AS num_actions,
    COALESCE(a.num_participation_stops, 0) AS num_participation_stops,
    COALESCE(a.num_session_restarts, 0) AS num_session_restarts,
    COALESCE(a.avg_bot_confidence, 0) AS avg_bot_confidence,

    COALESCE(pr.participation_percentage, 0) AS participation_percentage,

    COALESCE(mt.max_idle_time, '0 seconds'::interval) AS max_idle_time,

    COALESCE(bm.num_bot_messages, 0) +
    COALESCE(um.num_user_messages, 0) +
    COALESCE(al.num_active_loops, 0) +
    COALESCE(a.num_actions, 0) +
    COALESCE(sl.num_slots, 0) AS num_total_events
FROM filtered_sessions fs
LEFT JOIN bot_messages bm ON fs.session = bm.session
LEFT JOIN user_messages um ON fs.session = um.session
LEFT JOIN active_loops al ON fs.session = al.session
LEFT JOIN actions a ON fs.session = a.session
LEFT JOIN slots sl ON fs.session = sl.session
LEFT JOIN participation_ratios pr ON fs.session = pr.session
LEFT JOIN max_idle_times mt ON fs.session = mt.session
LEFT JOIN model_associations ma ON fs.session = ma.session
ORDER BY fs.session
