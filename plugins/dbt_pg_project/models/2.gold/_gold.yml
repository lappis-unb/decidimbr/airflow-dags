version: 2

models:
  - name: proposal_metrics  # Substitua pelo nome real do modelo SQL
    description: "Este modelo consolida informações sobre propostas, seus votos, comentários e bounce rate associados a cada dia desde a criação da proposta."
    columns:
      - name: data_operacao
        description: "Data da operação, correspondente à série temporal gerada para cada proposta."
        tests:
          - not_null

      - name: id_processo
        description: "ID do processo ao qual a proposta está associada."
        tests:
          - not_null

      - name: id_proposta
        description: "Identificador único da proposta."
        tests:
          - not_null

      - name: titulo_proposta
        description: "Título da proposta."
        tests:
        - not_null

      - name: status_proposta
        description: "Status atual da proposta."

      - name: titulo_processo
        description: "Título do processo participativo."
        tests:
        - not_null

      - name: eixo_tematico
        description: "Categoria temática à qual a proposta pertence."

      - name: qtd_rejeicao
        description: "Número de rejeições recebidas pela proposta em uma determinada data. Uma rejeição é contabilizada quando uma visita realiza apenas uma ação na página"
        tests:
          - not_null

      - name: qtd_visitas
        description: "Número de visitas na página"
        tests:
          - not_null


      - name: qtd_votos
        description: "Quantidade de votos recebidos pela proposta em uma determinada data."
        tests:
          - not_null

      - name: qtd_comentarios
        description: "Quantidade de comentários recebidos pela proposta em uma determinada data."
        tests:
          - not_null

  - name: visits_metrics
    description: "Métricas de visitas, incluindo contagem de visitantes, contagem de usuários e tempo médio de visita em minutos por dia."
    columns:
      - name: date_day
        description: "Data da visita ou do cadastro do usuário."
        tests:
          - not_null
          - unique

      - name: visitor_count
        description: "Contagem de visitantes únicos por dia."
        tests:
          - not_null

      - name: user_count
        description: "Contagem de novos usuários registrados por dia."
        tests:
          - not_null

      - name: visit_time_spend_in_minutes
        description: "Tempo médio de permanência dos visitantes no site, em minutos."
        tests:
          - not_null

  - name: device_entry_channel
    description: "Métricas relacionadas às visitas de usuários, incluindo duração total da visita, canal de entrada, tipo de dispositivo, contagem de visitantes únicos e tempo médio de visita em minutos por dia."
    columns:
      - name: day
        description: "Data da visita."
        tests:
          - not_null

      - name: entry_channel
        description: "Tipo de canal de entrada pelo qual o visitante acessou o site (ex: direto, referência)."
        tests:
          - not_null

      - name: device
        description: "Tipo de dispositivo utilizado pelo visitante (ex: celular, desktop)."
        tests:
          - not_null

      - name: unique_visitors
        description: "Contagem de visitantes únicos por dia, canal de entrada e dispositivo."
        tests:
          - not_null

      - name: avg_visit_time_in_minutes
        description: "Tempo médio gasto no site pelos visitantes, em minutos."
        tests:
          - not_null

  - name: fact_rasa_session
    description: "Métricas relacionadas as sessões dos usuários do BOT Rasa."
    columns:
      - name: session_id
        description: "Identificador único da sessão."

      - name: user_id
        description: "Identificador do usuário associado à sessão."

      - name: model_id
        description: "Identificador do modelo Rasa utilizado na sessão."

      - name: data_started
        description: "Data de início da sessão."

      - name: start_timestamp
        description: "Timestamp do início da sessão."

      - name: end_session_timestamp
        description: "Timestamp do término da sessão."

      - name: session_duration
        description: "Duração da sessão em segundos."

      - name: num_bot_messages
        description: "Número total de mensagens enviadas pelo bot durante a sessão."

      - name: num_invalid_votes
        description: "Número de votos inválidos registrados na sessão."

      - name: num_communication_errors
        description: "Número de erros de comunicação registrados na sessão."

      - name: num_user_messages
        description: "Número total de mensagens enviadas pelo usuário durante a sessão."

      - name: num_help_request
        description: "Número de solicitações de ajuda feitas pelo usuário."

      - name: num_active_loops
        description: "Número de fluxos ativos durante a sessão."

      - name: num_interrupted_flows
        description: "Número de fluxos interrompidos na sessão."

      - name: num_authentication_attempts
        description: "Número de tentativas de autenticação realizadas na sessão."

      - name: num_slots
        description: "Número total de slots registrados na sessão."

      - name: num_votes
        description: "Número de votos registrados na sessão."

      - name: num_profile_questions_sent
        description: "Número de perguntas de perfil enviadas na sessão."

      - name: num_comments_sent
        description: "Número de comentários enviados na sessão."

      - name: num_actions
        description: "Número total de ações realizadas na sessão."

      - name: num_participation_stops
        description: "Número de interrupções de participação registradas na sessão."

      - name: num_session_restarts
        description: "Número de reinícios de sessão registrados."

      - name: avg_bot_confidence
        description: "Confiança média do bot nas ações realizadas na sessão."

      - name: participation_percentage
        description: "Porcentagem de participação do usuário na sessão."

      - name: max_idle_time
        description: "Tempo máximo de inatividade do usuário durante a sessão."

      - name: num_total_events
        description: "Número total de eventos registrados na sessão."

# Testes adicionais para garantir integridade e qualidade dos dados
tests:
  - name: unique_proposal_per_day
    description: "Valida que cada proposta tem uma única entrada por dia."
    config:
      severity: error
    columns:
      - id_proposta
      - data_operacao
