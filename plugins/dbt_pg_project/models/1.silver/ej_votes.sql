{{ config(
    meta ={ "datasets_trigger": [ "bronze_ej_users_user", "bronze_ej_conversations_vote", "bronze_ej_conversations_comment", "bronze_ej_conversations_conversation" ] }
) }}

WITH resultado_enquetes AS (

    SELECT
        ej_users.id AS usuario_id,
        comments.id as comment_id,
        conv.id as conversation_id,
        comments.content AS comentario,
        votes.channel AS canal_voto,
        votes.created AS data_voto,
        votes.choice AS codigo_escolha,
        CASE
            WHEN votes.choice = 1 THEN 'agree'
            WHEN votes.choice = 0 THEN 'skip'
            WHEN votes.choice = -1 THEN 'disagree'
        END AS descricao_escolha,
        conv.title AS titulo_enquete,
        conv.text AS texto_pergunta
    FROM
        {{ source(
            'bronze',
            'ej_users_user'
        ) }}
        ej_users
        INNER JOIN {{ source(
            'bronze',
            'ej_conversations_vote'
        ) }}
        votes
        ON votes.author_id = ej_users.id
        INNER JOIN {{ source(
            'bronze',
            'ej_conversations_comment'
        ) }} comments
        ON comments.id = votes.comment_id
        INNER JOIN {{ source(
            'bronze',
            'ej_conversations_conversation'
        ) }}
        conv
        ON conv.id = comments.conversation_id
    WHERE
        votes.created :: DATE > '2024-01-10'
    ORDER BY
        votes.created DESC
)
SELECT
    usuario_id as id_usuario,
    conversation_id as ej_id_enquete,
	comment_id as ej_id_pergunta,
    codigo_escolha,
    descricao_escolha,
    comentario as pergunta_enquete,
    canal_voto,
    data_voto
FROM
    resultado_enquetes
