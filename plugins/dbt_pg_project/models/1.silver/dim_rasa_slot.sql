{{ config(
    materialized='incremental',
    schema='silver',
    unique_key="sk",
    indexes=[
      {'columns': ['session']}
      ]
) }}

WITH filtered_sessions AS (
    {{ filtered_sessions('dim_rasa_session') }}
),

parsed_events AS (
    SELECT
        id AS event_id,
        TO_TIMESTAMP(timestamp) AS datetime,
        sender_id as user_id,
        (data::json) ->> 'name' AS name,
        (data::json) ->> 'value' AS value,
        sender_id AS number
    FROM {{ source('bronze', 'events') }}
    WHERE "type_name" = 'slot'
),

associated_slot_events AS (
    SELECT
        pe.*,
        s.session_sk AS session,
        s.start_timestamp,
        s.end_timestamp
    FROM parsed_events pe
    LEFT JOIN filtered_sessions s
    ON pe.user_id = s.user_id
       AND pe.datetime >= s.start_timestamp
       AND pe.datetime <= s.end_timestamp
),

latest_events AS (
    SELECT
        session,
        number,
        name,
        MAX(datetime) AS latest_datetime
    FROM associated_slot_events
    GROUP BY session, number, name
),

sk_start AS (
    SELECT
        {% if is_incremental() %}
            COALESCE(MAX(sk), 0) AS current_max_sk
        FROM {{ this }}
        {% else %}
            0 AS current_max_sk
        {% endif %}
),

events_with_sk AS (
    SELECT
        ROW_NUMBER() OVER () + (SELECT current_max_sk FROM sk_start) AS sk,
        pe.event_id,
        le.latest_datetime AS datetime,
        pe.name,
        pe.value,
        pe.number,
        le.session
    FROM latest_events le
    JOIN associated_slot_events pe
    ON le.number = pe.number
       AND le.name = pe.name
       AND le.latest_datetime = pe.datetime
)

SELECT
    sk,
    session,
    event_id,
    datetime,
    name,
    value,
    number
FROM events_with_sk
ORDER BY sk
