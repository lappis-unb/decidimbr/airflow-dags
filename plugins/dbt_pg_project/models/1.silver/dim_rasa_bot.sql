{{ config(
    materialized='incremental',
    schema='silver',
    unique_key="sk",
    indexes=[
      {'columns': ['session']}
      ]
) }}

WITH filtered_sessions AS (
    {{ filtered_sessions('dim_rasa_session') }}
),

parsed_bot_events AS (
    SELECT
        id AS event_id,
        TO_TIMESTAMP(timestamp) AS datetime,
        sender_id as user_id,
        (data::json -> 'metadata') ->> 'utter_action' AS utter_action,
        (data::json -> 'metadata') ->> 'template' AS template,
        (data::json) ->> 'text' AS text,
        (data::json) AS raw_data
    FROM {{ source('bronze', 'events') }}
    WHERE type_name = 'bot'
),

associated_bot_events AS (
    SELECT
        pbe.*,
        s.session_sk AS session,
        s.start_timestamp,
        s.end_timestamp
    FROM parsed_bot_events pbe
    LEFT JOIN filtered_sessions s
    ON pbe.user_id = s.user_id
       AND pbe.datetime >= s.start_timestamp
       AND pbe.datetime <= s.end_timestamp
),

sk_start AS (
    SELECT
        {% if is_incremental() %}
            COALESCE(MAX(sk), 0) AS current_max_sk
        FROM {{ this }}
        {% else %}
            0 AS current_max_sk
        {% endif %}
),

events_with_sk AS (
    SELECT
        ROW_NUMBER() OVER () + (SELECT current_max_sk FROM sk_start) AS sk,
        abe.*
    FROM associated_bot_events abe
)

SELECT
    sk,
    session,
    event_id,
    template,
    text,
    raw_data AS data,
    utter_action,
    datetime
FROM events_with_sk
ORDER BY sk
