{{ config(
    materialized='incremental',
    schema='silver',
    unique_key="sk",
    indexes=[
      {'columns': ['session']}
      ]
) }}

WITH filtered_sessions AS (
    {{ filtered_sessions('dim_rasa_session') }}
),

filtered_active_loops AS (
    SELECT
        id AS event_id,
        sender_id AS number,
        TO_TIMESTAMP(timestamp) AS datetime,
        action_name AS form_name
    FROM {{ source('bronze', 'events') }}
    WHERE type_name = 'active_loop'
),

associated_active_loops AS (
    SELECT
        fal.*,
        s.session_sk AS session,
        s.start_timestamp,
        s.end_timestamp
    FROM filtered_active_loops fal
    LEFT JOIN filtered_sessions s
    ON fal.number = s.user_id
       AND fal.datetime >= s.start_timestamp
       AND fal.datetime <= s.end_timestamp
),

sk_start AS (
    SELECT
        {% if is_incremental() %}
            COALESCE(MAX(sk), 0) AS current_max_sk
        FROM {{ this }}
        {% else %}
            0 AS current_max_sk
        {% endif %}
),

active_loops_with_sk AS (
    SELECT
        ROW_NUMBER() OVER () + (SELECT current_max_sk FROM sk_start) AS sk,
        aal.*
    FROM associated_active_loops aal
)

SELECT
    sk,
    session,
    event_id,
    number,
    form_name,
    datetime
FROM active_loops_with_sk
ORDER BY sk
