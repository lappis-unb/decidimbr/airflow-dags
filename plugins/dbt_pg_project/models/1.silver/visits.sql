{{ config(
   materialized='table',
   meta={
      "datasets_trigger": [
         "bronze_matomo_detailed_visits"
      ]
   }
) }}

SELECT 
    pp.process_id,
    a.*
FROM (
    SELECT
         CASE 
             WHEN ((mdv."actionDetails"::jsonb)->0->>'url') ~* '\/processes\/(\w+)\/?' 
             THEN regexp_replace((mdv."actionDetails"::jsonb)->0->>'url', '^.*\/processes\/(\w+)\/?.*$', '\1')
             ELSE NULL 
         END AS participatory_process_slug,
         cast(CASE 
             WHEN ((mdv."actionDetails"::jsonb)->0->>'url') ~* '\/proposals\/(\d+)\/?'
             THEN regexp_replace((mdv."actionDetails"::jsonb)->0->>'url', '^.*\/proposals\/(\d+)\/?.*$', '\1')
             ELSE NULL 
         end as int8) AS proposal_id,
         mdv."idVisit"                  AS session_id,
         mdv."visitIp"                  AS visit_ip,
         mdv."visitorId"                AS visitor_id,
         mdv."goalConversions"          AS goal_conversions,
         mdv."serverDate"::DATE         AS session_date,
         mdv."visitServerHour"          AS visit_server_hour,
         mdv."lastActionTimestamp"      AS last_action_timestamp,
         mdv."lastActionDateTime"       AS last_action_date_time,
         mdv."serverTimestamp"          AS server_timestamp,
         mdv."firstActionTimestamp"     AS first_action_timestamp,
         mdv."userId"                   AS user_id,
         mdv."visitorType"              AS visitor_type,
         mdv."visitConverted"           AS visit_converted,
         mdv."visitCount"               AS visit_count,
         mdv."daysSinceFirstVisit"      AS days_since_first_visit,
         mdv."secondsSinceFirstVisit"   AS seconds_since_first_visit,
         mdv."visitDuration"            AS visit_duration_seconds,
         mdv.searches                   AS searches,
         mdv.events                     AS events,
         mdv.continent                  AS continent,
         mdv."continentCode"            AS continent_code,
         mdv.country                    AS country,
         mdv."countryCode"              AS country_code,
         mdv.region                     AS region,
         mdv."deviceType"               AS device_type,
         mdv."regionCode"               AS region_code,
         mdv.city                       AS city,
         mdv."location"                 AS location,
         mdv."visitLocalHour"           AS visit_local_hour,
         mdv."daysSinceLastVisit"       AS days_since_last_visit,
         mdv."secondsSinceLastVisit"    AS seconds_since_last_visit,
         mdv.resolution                 AS resolution,
         mdv."actions"                  AS num_actions,
         mdv."referrerType"             AS referrer_type,
         mdv."referrerTypeName"         AS referrer_type_name,
         mdv."referrerName"             AS referrer_name,
         mdv."referrerUrl"              AS referrer_url,
         (mdv."actionDetails"::jsonb)->0->>'url' AS url
    FROM {{ source('bronze', 'matomo_detailed_visits') }} mdv
) AS a
LEFT JOIN {{ ref('participatory_processes') }} AS pp
    ON a.participatory_process_slug = pp.process_slug