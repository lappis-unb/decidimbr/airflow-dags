{{ config(
    materialized="incremental",
    schema="silver",
    unique_key="sk"
) }}

WITH distinct_models AS (
    SELECT
        DISTINCT
        data::json->'metadata'->>'model_id' AS model_id,
        data::json->'metadata'->>'assistant_id' AS assistant_id
    FROM
        {{ source('bronze', 'events') }}
),

filtered_models AS (
    SELECT
        dm.*
    FROM distinct_models dm
    {% if is_incremental() %}
    LEFT JOIN {{ this }} existing
    ON dm.model_id = existing.model_id
       AND dm.assistant_id = existing.assistant_id
    WHERE existing.model_id IS NULL
    {% endif %}
),

sk_start AS (
    SELECT
        {% if is_incremental() %}
            COALESCE(MAX(sk), 0) AS current_max_sk
        FROM {{ this }}
        {% else %}
            0 AS current_max_sk
        {% endif %}
),

models_with_sk AS (
    SELECT
        ROW_NUMBER() OVER () + (SELECT current_max_sk FROM sk_start) AS sk,
        fm.*
    FROM filtered_models fm
)

SELECT
    sk,
    model_id,
    assistant_id
FROM models_with_sk
ORDER BY sk
