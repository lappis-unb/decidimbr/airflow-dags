{{ config(
    materialized="incremental",
    schema="silver",
    unique_key="sk",
    indexes=[
      {'columns': ['session']}
      ]
) }}

WITH filtered_sessions AS (
    {{ filtered_sessions('dim_rasa_session') }}
),

parsed_user_events AS (
    SELECT
        id AS event_id,
        data::json->>'message_id' AS message_id,
        sender_id AS number,
        intent_name,
        data::json->>'text' AS user_text,
        data::json->>'parse_data' AS parse_data,
        data::json->>'input_channel' AS input_channel,
        TO_TIMESTAMP(timestamp) AS datetime
    FROM {{ source('bronze', 'events') }}
    WHERE type_name = 'user'
),

associated_user_events AS (
    SELECT
        pue.*,
        s.session_sk AS session,
        s.start_timestamp,
        s.end_timestamp
    FROM parsed_user_events pue
    LEFT JOIN filtered_sessions s
    ON pue.number = s.user_id
       AND pue.datetime >= s.start_timestamp
       AND pue.datetime <= s.end_timestamp
),

sk_start AS (
    SELECT
        {% if is_incremental() %}
            COALESCE(MAX(sk), 0) AS current_max_sk
        FROM {{ this }}
        {% else %}
            0 AS current_max_sk
        {% endif %}
),

events_with_sk AS (
    SELECT
        ROW_NUMBER() OVER () + (SELECT current_max_sk FROM sk_start) AS sk,
        aue.*
    FROM associated_user_events aue
)

SELECT
    sk,
    session,
    event_id,
    message_id,
    number,
    intent_name,
    user_text,
    parse_data,
    input_channel,
    datetime
FROM
    events_with_sk
ORDER BY sk
