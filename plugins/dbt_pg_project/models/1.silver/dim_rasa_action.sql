{{ config(
    materialized="incremental",
    schema="silver",
    unique_key="sk",
    indexes=[
      {'columns': ['session']}
      ]
) }}

WITH filtered_sessions AS (
    {{ filtered_sessions('dim_rasa_session') }}
),

parsed_action_events AS (
    SELECT
        id AS event_id,
        action_name,
        sender_id as user_id,
        data::json->>'policy' AS policy,
        (data::json->>'confidence')::float AS confidence,
        data::json->>'action_text' AS action_text,
        (data::json->>'hide_rule_turn')::boolean AS hide_rule_turn,
        TO_TIMESTAMP(timestamp) AS datetime
    FROM {{ source('bronze', 'events') }}
    WHERE type_name = 'action'
),

associated_action_events AS (
    SELECT
        pae.*,
        s.session_sk AS session,
        s.start_timestamp,
        s.end_timestamp
    FROM parsed_action_events pae
    LEFT JOIN filtered_sessions s
    ON pae.user_id = s.user_id
       AND pae.datetime >= s.start_timestamp
       AND pae.datetime <= s.end_timestamp
),

sk_start AS (
    SELECT
        {% if is_incremental() %}
            COALESCE(MAX(sk), 0) AS current_max_sk
        FROM {{ this }}
        {% else %}
            0 AS current_max_sk
        {% endif %}
),

actions_with_sk AS (
    SELECT
        ROW_NUMBER() OVER () + (SELECT current_max_sk FROM sk_start) AS sk,
        aae.*
    FROM associated_action_events aae
)

SELECT
    sk,
    session,
    event_id,
    action_name,
    policy,
    confidence,
    action_text,
    hide_rule_turn,
    datetime
FROM actions_with_sk
ORDER BY sk
