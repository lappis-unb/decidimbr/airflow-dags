{{ config(
   meta={
	"datasets_trigger": [
		"bronze_decidim_proposals_proposals",
		"bronze_decidim_scopes",
		"bronze_decidim_components",
		"bronze_decidim_coauthorships",
		"bronze_decidim_components",
		"bronze_decidim_categorizations",
		"bronze_decidim_categories"
	]
   }
) }}

WITH proposals_base AS (

    SELECT
        *,
        ROW_NUMBER() over(
            PARTITION BY id
            ORDER BY
                updated_at DESC
        ) AS ROW_NUMBER
    FROM
        {{ source(
            'bronze',
            'decidim_proposals_proposals'
        ) }}
),
deduped_proposals AS (
    SELECT
        *
    FROM
        proposals_base
    WHERE
        ROW_NUMBER = 1
),
scopes_base AS (
    SELECT
        *,
        ROW_NUMBER() over(
            PARTITION BY id,
            decidim_organization_id
            ORDER BY
                updated_at DESC
        ) AS ROW_NUMBER
    FROM
        {{ source(
            'bronze',
            'decidim_scopes'
        ) }}
    WHERE
        decidim_organization_id = 1 -- Filter ONLY brasil participativo
),
deduped_scopes AS (
    SELECT
        *
    FROM
        scopes_base
    WHERE
        ROW_NUMBER = 1
),
components_base AS (
    SELECT
        *,
        COALESCE(
            (
                dc.settings :: json -> 'global' ->> 'participatory_texts_enabled'
            ) :: BOOLEAN,
            FALSE
        ) AS is_participatory_text,
        ROW_NUMBER() over(
            PARTITION BY id
            ORDER BY
                updated_at DESC
        ) AS ROW_NUMBER
    FROM
        {{ source(
            'bronze',
            'decidim_components'
        ) }}
        dc
),
deduped_components AS (
    SELECT
        *
    FROM
        components_base
    WHERE
        ROW_NUMBER = 1
),
coauthorships_base AS (
    SELECT
        *,
        ROW_NUMBER() over(
            PARTITION BY decidim_author_id,
            coauthorable_id
            ORDER BY
                updated_at DESC
        ) AS ROW_NUMBER
    FROM
        {{ source(
            'bronze',
            'decidim_coauthorships'
        ) }}
    WHERE
        coauthorable_type = 'Decidim::Proposals::Proposal'
),
deduped_coauthorships AS (
    SELECT
        *
    FROM
        coauthorships_base
    WHERE
        ROW_NUMBER = 1
),
categorizations_base AS (
    SELECT
        *,
        ROW_NUMBER() over(
            PARTITION BY categorizable_id,
            decidim_category_id
            ORDER BY
                updated_at DESC
        ) AS ROW_NUMBER
    FROM
        {{ source(
            'bronze',
            'decidim_categorizations'
        ) }}
    WHERE
        categorizable_type = 'Decidim::Proposals::Proposal'
),
deduped_categorizations AS (
    SELECT
        *
    FROM
        categorizations_base
    WHERE
        ROW_NUMBER = 1
)
SELECT
    p.id AS proposal_id,
    p.decidim_component_id AS component_id,
    C.participatory_space_id AS process_id,
    C.is_participatory_text,
    C.participatory_space_type,
    ca.decidim_author_id AS user_id,
    ca.decidim_author_type AS author_type,
    p.state AS proposal_status,
    p.created_at,
    CASE
        WHEN p.title ~ '^[{].*[}]$' THEN p.title :: jsonb ->> 'pt-BR'
        ELSE p.title
    END AS proposal_title,
    CASE
        WHEN p.body ~ '^[{].*[}]$' THEN p.body :: jsonb ->> 'pt-BR'
        ELSE p.body
    END AS proposal_text,
    CASE
        WHEN s.name ~ '^[{].*[}]$' THEN s.name :: jsonb ->> 'pt-BR'
        ELSE s.name
    END AS proposal_scope,
    CASE
        WHEN cat.name ~ '^[{].*[}]$' THEN cat.name :: jsonb ->> 'pt-BR'
        ELSE cat.name
    END AS proposal_category
FROM
    deduped_proposals p
    LEFT JOIN deduped_scopes s
    ON p.decidim_scope_id = s.id
    LEFT JOIN deduped_components C
    ON p.decidim_component_id = C.id
    LEFT JOIN deduped_coauthorships ca
    ON p.id = ca.coauthorable_id
    LEFT JOIN deduped_categorizations pc
    ON p.id = pc.categorizable_id
    LEFT JOIN {{ source(
        'bronze',
        'decidim_categories'
    ) }}
    cat
    ON pc.decidim_category_id = cat.id
