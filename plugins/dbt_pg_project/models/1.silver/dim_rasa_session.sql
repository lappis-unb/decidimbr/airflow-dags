{{ config(
    materialized="incremental",
    schema="silver",
    unique_key="sk"
) }}

WITH filtered_events AS (
    SELECT
        id,
        TO_TIMESTAMP(timestamp) AS timestamp,
        sender_id,
        type_name
    FROM
        {{ source('bronze', 'events') }}
    WHERE
        (action_name IS NULL OR
        (action_name != 'session_started_metadata' AND action_name != 'action_session_start'))
),
{% if is_incremental() %}
last_sessions AS (
    SELECT
        user_id as sender_id,
        MAX(end_timestamp) AS last_session_end
    FROM
        {{ this }}
    GROUP BY
        user_id
),
{% else %}
last_sessions AS (
    SELECT
        NULL AS sender_id,
        NULL::timestamp AS last_session_end
    WHERE 1=0
),
{% endif %}

new_events AS (
    SELECT
        fe.*
    FROM
        filtered_events fe
    LEFT JOIN
        last_sessions ls
    ON
        fe.sender_id = ls.sender_id
    WHERE
        ls.last_session_end IS NULL OR fe.timestamp >= ls.last_session_end
),

sessions_started AS (
    SELECT
        id AS session_id,
        sender_id,
        timestamp AS start_timestamp
    FROM
        new_events
    WHERE
        type_name = 'session_started'
),

session_boundaries AS (
    SELECT
        ss.sender_id,
        ss.session_id AS start_session_id,
        ss.start_timestamp,
        LEAD(ss.start_timestamp) OVER (
            PARTITION BY ss.sender_id ORDER BY ss.start_timestamp
        ) AS next_session_start
    FROM
        sessions_started ss
),

session_events AS (
    SELECT
        sb.sender_id,
        sb.start_session_id,
        sb.start_timestamp,
        fe.id AS event_id,
        fe.timestamp AS event_timestamp
    FROM
        session_boundaries sb
    LEFT JOIN
        new_events fe
    ON
        sb.sender_id = fe.sender_id
        AND fe.timestamp >= sb.start_timestamp
        AND (sb.next_session_start IS NULL OR fe.timestamp < sb.next_session_start)
),

max_session_events AS (
    SELECT
        se.sender_id,
        se.start_session_id,
        MAX(se.event_id) AS max_event_id,
        MAX(se.event_timestamp) AS max_event_timestamp
    FROM
        session_events se
    GROUP BY
        se.sender_id, se.start_session_id
),

validated_sessions AS (
    SELECT
        mse.sender_id,
        mse.start_session_id AS session_id,
        sb.start_timestamp,
        mse.max_event_id AS end_session_id,
        mse.max_event_timestamp AS end_timestamp
    FROM
        max_session_events mse
    JOIN
        session_boundaries sb
    ON
        mse.sender_id = sb.sender_id
        AND mse.start_session_id = sb.start_session_id
    WHERE
        NOW() - mse.max_event_timestamp > INTERVAL '1 hour'
),

sk_start AS (
    SELECT
        {% if is_incremental() %}
            COALESCE(MAX(sk), 0) AS current_max_sk
        FROM
            {{ this }}
        {% else %}
            0 AS current_max_sk
        {% endif %}
),

sessions_with_sk AS (
    SELECT
        ROW_NUMBER() OVER () + (SELECT current_max_sk FROM sk_start) AS sk,
        vs.*
    FROM
        validated_sessions vs
)

SELECT
    sk,
    sender_id AS user_id,
    session_id AS start,
    start_timestamp,
    end_session_id AS "end",
    end_timestamp
FROM
    sessions_with_sk
ORDER BY
    user_id, start_timestamp
