{{ config(
   meta={
    "datasets_trigger": [
        "bronze_decidim_components",
        "bronze_decidim_participatory_processes",
        "bronze_decidim_participatory_process_types",
        "bronze_decidim_areas",
        "bronze_decidim_categories"
    ]
   }
) }}

WITH components AS (
    SELECT
        dc.id AS component_id,
        dc.participatory_space_id,
        dc.weight AS component_weight,
        dc.manifest_name,
        (dc.name::json)->>'pt-BR' AS component_name,
        dc.published_at,
        dc.created_at,
        dc.updated_at,
        SPLIT_PART(dc.participatory_space_type, '::', 2) AS participatory_space_type,
        dc.hide_in_menu,
        (dc.singular_name::json)->>'pt-BR' AS component_singular_name,
        (dc.menu_name::json)->>'pt-BR' AS component_menu_name
    FROM
        {{ source('bronze', 'decidim_components') }} dc
),
processes AS (
    SELECT
        dpp.id AS id,
        dpp.slug,
        (dpp.title::json)->>'pt-BR' AS title,
        (dpp.subtitle::json)->>'pt-BR' AS subtitle,
        REGEXP_REPLACE((dpp.short_description::json)->>'pt-BR', '<[^>]+>', '', 'g') AS short_description,
        REGEXP_REPLACE((dpp.description::json)->>'pt-BR', '<[^>]+>', '', 'g') AS participatory_processes_description,
        dpp.hero_image,
        dpp.banner_image,
        dpp.promoted,
        (dpp.developer_group::json)->>'pt-BR' AS developer_group,
        dpp.end_date,
        (dpp.meta_scope::json)->>'pt-BR' AS meta_scope,
        (dpp.local_area::json)->>'pt-BR' AS local_area,
        (dpp.target::json)->>'pt-BR' AS target,
        (dpp.participatory_scope::json)->>'pt-BR' AS participatory_scope,
        (dpp.participatory_structure::json)->>'pt-BR' AS participatory_structure,
        dpp.decidim_scope_id,
        dpp.decidim_participatory_process_group_id,
        dpp.show_statistics,
        (dpp.announcement::json)->>'pt-BR' AS announcement,
        dpp.scopes_enabled,
        dpp.start_date,
        dpp.private_space,
        dpp.reference,
        dpp.decidim_area_id,
        dpp.decidim_scope_type_id,
        dpp.show_metrics,
        dpp.weight AS weight,
        dpp.follows_count,
        dpp.decidim_participatory_process_type_id,
        dpp.initial_page_type,
        dpp.initial_page_component_id,
        dpp.group_chat_id,
        dpp.should_have_user_full_profile,
        dpp.publish_date,
        dpp.show_mobilization,
        dpp.is_template,
        dpp.mobilization_title,
        dpp.mobilization_position
    FROM
        {{ source('bronze', 'decidim_participatory_processes') }} dpp
),
process_types AS (
    SELECT
        dppt.id AS dppt_id,
        (dppt.title::json)->>'pt-BR' AS participatory_process_type_title
    FROM
        {{ source('bronze', 'decidim_participatory_process_types') }} dppt
),
areas AS (
    SELECT
        da.id AS da_id,
        (da.name::json)->>'pt-BR' AS area_name
    FROM
        {{ source('bronze', 'decidim_areas') }} da
)
SELECT
    c.component_id,
    c.participatory_space_id,
    c.component_weight,
    c.manifest_name,
    c.component_name,
    c.published_at,
    c.created_at,
    c.updated_at,
    c.participatory_space_type,
    c.hide_in_menu,
    c.component_singular_name,
    c.component_menu_name,
    p.id,
    p.slug,
    p.title,
    p.subtitle,
    p.short_description,
    p.participatory_processes_description,
    p.hero_image,
    p.banner_image,
    p.promoted,
    p.developer_group,
    p.end_date,
    p.meta_scope,
    p.local_area,
    p.target,
    p.participatory_scope,
    p.participatory_structure,
    p.decidim_scope_id,
    p.decidim_participatory_process_group_id,
    p.show_statistics,
    p.announcement,
    p.scopes_enabled,
    p.start_date,
    p.private_space,
    p.reference,
    p.decidim_area_id,
    p.decidim_scope_type_id,
    p.show_metrics,
    p.weight,
    p.follows_count,
    p.decidim_participatory_process_type_id,
    p.initial_page_type,
    p.initial_page_component_id,
    p.group_chat_id,
    p.should_have_user_full_profile,
    p.publish_date,
    p.show_mobilization,
    p.is_template,
    p.mobilization_title,
    p.mobilization_position,
    pt.participatory_process_type_title,
    a.area_name
FROM
    components c
JOIN
    processes p
        ON c.participatory_space_id = p.id
LEFT JOIN
    process_types pt
        ON p.decidim_participatory_process_type_id = pt.dppt_id
LEFT JOIN
    areas a
        ON p.decidim_area_id = a.da_id
