{% macro filtered_sessions(source_table) %}
WITH sessions AS (
    SELECT
        sk AS session_sk,
        user_id,
        DATE(start_timestamp) AS data_started,
        start_timestamp,
        end_timestamp,
        (end_timestamp - start_timestamp) AS session_duration
    FROM {{ ref(source_table) }}
),

last_processed_sk AS (
    SELECT
        {% if is_incremental() %}
            COALESCE(MAX(session), 0) AS last_session_sk
        FROM {{ this }}
        {% else %}
            0 AS last_session_sk
        {% endif %}
)

SELECT
    *
FROM sessions
WHERE session_sk > (SELECT last_session_sk FROM last_processed_sk)
{% endmacro %}
