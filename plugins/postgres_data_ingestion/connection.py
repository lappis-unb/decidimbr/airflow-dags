import psycopg2
import psycopg2.extras


def get_connection(config):
    """Establishes a connection to the PostgreSQL database.

    This function creates a connection to a PostgreSQL database using
    the provided configuration dictionary. It specifically utilizes
    the `LogicalReplicationConnection` from psycopg2.extras for logical
    replication purposes.

    Args:
    ----
        config (dict): A dictionary containing the database connection parameters.

    Exemple:
        ```python
            config = {
                "host": '192.168.1.1'
                "port": 5432
                "user": 'postgres'
                "password": 'postgres'
                "database": 'postgres'
            }
        ```

    Returns:
    -------
        (psycopg2.extensions.connection): A PostgreSQL connection object.

    Raises:
    ------
        (psycopg2.OperationalError): If the connection fails due to invalid
        configuration or connectivity issues.
    """
    return psycopg2.connect(
        host=config["host"],
        port=config["port"],
        user=config["user"],
        password=config["password"],
        dbname=config["database"],
        connection_factory=psycopg2.extras.LogicalReplicationConnection,
    )


def close_connection(cur, conn):
    """Closes the database cursor and connection.

    This function safely closes the provided cursor and connection to
    release resources.

    Args:
    ----
        cur (psycopg2.extensions.cursor): The cursor object to close.
        conn (psycopg2.extensions.connection): The connection object to close.

    Returns:
    -------
        (None): Returns None

    Raises:
    ------
        psycopg2.Error: If an error occurs during cursor or connection closure.
    """
    cur.close()
    conn.close()
