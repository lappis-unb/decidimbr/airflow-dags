from typing import TYPE_CHECKING

from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.providers.ssh.hooks.ssh import SSHHook
from sqlalchemy import create_engine

if TYPE_CHECKING:
    from sshtunnel import SSHTunnelForwarder


def create_tunnel_engine(db, ssh_conn: str, ssh_tunnel: bool = False):
    """
    Cria um engine SQLAlchemy e um tunel SSH opcional.

    Args:
    ----
        db (Connection): Objeto de conexão do banco de dados recuperado via Airflow PostgresHook.
        ssh_conn (str): ID da conexão SSH configurada no Airflow.
        ssh_tunnel (bool, optional): Flag para ativar/desativar o túnel SSH. Default é False.

    Returns:
    -------
        tuple:
            - SSHTunnelForwarder | None: Objeto de túnel SSH caso `ssh_tunnel` seja True.
            - Engine: Objeto SQLAlchemy Engine configurado para o banco de dados.

    Example:
    -------
        >>> db_conn = PostgresHook.get_connection("my_db_conn")
        >>> tunnel, engine = create_tunnel_engine(db_conn, "ssh_default", ssh_tunnel=True)
    """
    if ssh_tunnel:
        tunnel = SSHHook(ssh_conn).get_tunnel(remote_host=db.host, remote_port=db.port)
        tunnel.start()
        tunnel: SSHTunnelForwarder
        engine = create_engine(
            f"postgresql://{db.login}:{db.password}@127.0.0.1:{tunnel.local_bind_port}/{db.schema}"
        )

        return tunnel, engine
    else:
        connection_string = f"postgresql://{db.login}:{db.password}@{db.host}:{db.port}/{db.schema}"
        engine = create_engine(connection_string)

        return None, engine


def setup_ssh_tunnel_and_engine(db_conn_id: str, ssh_conn: str, ssh_tunnel: bool = False):
    """
    Configura o túnel SSH opcional e cria um SQLAlchemy engine para conexão com o banco de dados.

    Esta função é uma utilitária para configurações de conexões seguras com o banco
    de dados via Airflow. Utiliza o PostgresHook para obter credenciais de conexão do Airflow
    e o SSHHook para criar túneis SSH, se ativado.

    Args:
    ----
        db_conn_id (str): ID da conexão do banco de dados configurada no Airflow.
        ssh_conn (str): ID da conexão SSH configurada no Airflow.
        ssh_tunnel (bool, optional): Flag para ativar/desativar o túnel SSH. Por padrão é False.

    Returns:
    -------
        tuple:
            - SSHTunnelForwarder | None: Túnel SSH ativo caso `ssh_tunnel` seja True.
            - Engine: Objeto SQLAlchemy configurado com as credenciais do banco.

    Example:
    -------
        >>> tunnel, engine = setup_ssh_tunnel_and_engine("my_db_conn", "ssh_default", ssh_tunnel=True)
        >>> with engine.connect() as connection:
        >>>     result = connection.execute("SELECT * FROM my_table")
        >>>     print(result.fetchall())
    """
    db = PostgresHook.get_connection(db_conn_id)
    tunnel, engine = create_tunnel_engine(db=db, ssh_conn=ssh_conn, ssh_tunnel=ssh_tunnel)
    return tunnel, engine
