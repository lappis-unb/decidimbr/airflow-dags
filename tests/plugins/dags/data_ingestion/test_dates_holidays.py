from datetime import date
from unittest.mock import MagicMock, patch

import pandas as pd
import pytest

from dags.data_ingestion.dates_holidays import (
    _create_dataframe,
    _get_holidays,
    calc_week_number_of_year,
    load,
    week_number_of_month,
)


@pytest.mark.data_ingestion
@pytest.mark.parametrize(
    "test_date, week_day_num, expected",
    [
        (date(2024, 12, 4), 0, 1),
        (date(2024, 12, 10), 0, 2),
        (date(2024, 12, 17), 0, 3),
        (date(2024, 12, 25), 0, 4),
        (date(2024, 12, 31), 0, 5),
        (date(2024, 11, 1), 0, 1),
        (date(2024, 11, 8), 4, 2),
    ],
)
def test_week_number_of_month(test_date, week_day_num, expected):
    result = week_number_of_month(test_date, week_day_num)
    assert (
        result == expected
    ), f"Failed for {test_date} with week_day_num={week_day_num}: got {result}, expected {expected}"


@pytest.mark.parametrize(
    "day_of_year, week_day_num, expected",
    [
        # Testes básicos
        (1, 1, 1),
        (7, 1, 2),
        (8, 1, 2),
        (15, 1, 3),
        (365, 1, 53),
        # Testes com diferentes week_day_num
        (1, 7, 0),
        (7, 7, 1),
        (8, 7, 1),
        (15, 7, 2),
        (365, 7, 52),
        # Testes de ano bissexto
        (366, 1, 53),
        # Testes de limites
        (0, 1, 1),
        (1, 0, (1 - 0 + 10) // 7),
        (1, 8, (1 - 8 + 10) // 7),
    ],
)
def test_calc_week_number_of_year(day_of_year, week_day_num, expected):
    assert calc_week_number_of_year(day_of_year, week_day_num) == expected


@pytest.fixture
def sample_dataframe():
    data = {"column1": [1, 2, 3], "column2": ["a", "b", "c"]}
    return pd.DataFrame(data)


@patch("dags.data_ingestion.dates_holidays.PostgresHook", autospec=True)
@patch.object(pd.DataFrame, "to_sql")
def test_load(mock_to_sql, mock_postgres_hook, sample_dataframe):
    mock_engine = MagicMock()
    mock_connection = MagicMock()
    mock_engine.connect.return_value.__enter__.return_value = mock_connection
    mock_postgres_hook.return_value.get_sqlalchemy_engine.return_value = mock_engine
    table_name = "holidays"
    schema = "raw"
    load(sample_dataframe, table_name, schema)
    mock_postgres_hook.assert_called_once_with(postgres_conn_id="pg_bp_analytics")
    mock_postgres_hook.return_value.get_sqlalchemy_engine.assert_called_once()
    mock_engine.connect.assert_called_once()
    mock_connection.execute.assert_called_once_with(f"DROP TABLE IF EXISTS {schema}.{table_name} CASCADE;")
    mock_to_sql.assert_called_once_with(
        table_name, con=mock_engine, schema=schema, if_exists="replace", index=False
    )


def test_create_dataframe():
    with patch("dags.data_ingestion.dates_holidays.START_DATE", "2023-01-01"), patch(
        "dags.data_ingestion.dates_holidays.END_DATE", "2023-01-07"
    ), patch(
        "dags.data_ingestion.dates_holidays.MONTHS",
        {
            1: "janeiro",
            2: "fevereiro",
            3: "março",
            4: "abril",
            5: "maio",
            6: "junho",
            7: "julho",
            8: "agosto",
            9: "setembro",
            10: "outubro",
            11: "novembro",
            12: "dezembro",
        },
    ), patch(
        "dags.data_ingestion.dates_holidays.WEEK_DAYS",
        {
            0: "segunda-feira",
            1: "terça-feira",
            2: "quarta-feira",
            3: "quinta-feira",
            4: "sexta-feira",
            5: "sábado",
            6: "domingo",
        },
    ), patch(
        "dags.data_ingestion.dates_holidays.week_number_of_month"
    ) as mock_week_number_of_month, patch(
        "dags.data_ingestion.dates_holidays.calc_week_number_of_year"
    ) as mock_calc_week_number_of_year:
        mock_week_number_of_month.side_effect = lambda date, dow: (date.day - 1) // 7 + 1
        mock_calc_week_number_of_year.side_effect = lambda day_of_year, dow: (day_of_year - 1) // 7 + 1
        df = _create_dataframe()
        expected_columns = [
            "year_month_day",
            "year_month",
            "year",
            "week_day_description",
            "month_description",
            "month_number",
            "day_of_month",
            "day_of_week",
            "day_of_year",
            "week_of_month",
            "weekend",
            "week_of_year",
        ]
        assert (
            list(df.columns) == expected_columns
        ), f"Expected columns {expected_columns}, but got {list(df.columns)}"
        assert len(df) == 7, f"Expected 7 rows, but got {len(df)}"
        expected_first_row = {
            "year_month_day": "20230101",
            "year_month": "202301",
            "year": "2023",
            "week_day_description": "domingo",
            "month_description": "janeiro",
            "month_number": 1,
            "day_of_month": 1,
            "day_of_week": 1,
            "day_of_year": 1,
            "week_of_month": 1,
            "weekend": True,
            "week_of_year": 1,
        }
        actual_first_row = df.iloc[0].to_dict()
        assert (
            actual_first_row == expected_first_row
        ), f"First row mismatch.\nExpected: {expected_first_row}\nGot: {actual_first_row}"
        expected_last_row = {
            "year_month_day": "20230107",
            "year_month": "202301",
            "year": "2023",
            "week_day_description": "sábado",
            "month_description": "janeiro",
            "month_number": 1,
            "day_of_month": 7,
            "day_of_week": 7,
            "day_of_year": 7,
            "week_of_month": 1,
            "weekend": True,
            "week_of_year": 1,
        }
        actual_last_row = df.iloc[-1].to_dict()
        assert (
            actual_last_row == expected_last_row
        ), f"Last row mismatch.\nExpected: {expected_last_row}\nGot: {actual_last_row}"
        expected_middle_row = {
            "year_month_day": "20230104",
            "year_month": "202301",
            "year": "2023",
            "week_day_description": "quarta-feira",
            "month_description": "janeiro",
            "month_number": 1,
            "day_of_month": 4,
            "day_of_week": 4,
            "day_of_year": 4,
            "week_of_month": 1,
            "weekend": False,
            "week_of_year": 1,
        }
        actual_middle_row = df.iloc[3].to_dict()
        assert (
            actual_middle_row == expected_middle_row
        ), f"Middle row mismatch.\nExpected: {expected_middle_row}\nGot: {actual_middle_row}"


@pytest.mark.data_ingestion5
def test_get_holidays():
    with patch("dags.data_ingestion.dates_holidays.START_DATE", "2023-01-01"), patch(
        "dags.data_ingestion.dates_holidays.END_DATE", "2023-01-02"
    ), patch("dags.data_ingestion.dates_holidays.requests.get") as mock_get, patch(
        "dags.data_ingestion.dates_holidays.time.sleep"
    ) as mock_sleep:
        mock_response_data_2023 = (
            '[{"date": "2023-01-01", "name": "Confraternização Universal", "type": "national"}]'
        )
        mock_response = MagicMock()
        mock_response.text = mock_response_data_2023
        mock_response.raise_for_status.return_value = None
        mock_get.return_value = mock_response
        df = _get_holidays()
        mock_get.assert_called_once_with("https://brasilapi.com.br/api/feriados/v1/2023")
        assert mock_get.call_count == 1, "Esperado que requests.get seja chamado uma vez."
        assert mock_sleep.call_count == 1, "Esperado que time.sleep seja chamado uma vez."
        expected_df = pd.DataFrame(
            [{"date": "2023-01-01", "name": "Confraternização Universal", "type": "national"}]
        )
        expected_df["date"] = pd.to_datetime(expected_df["date"])
        pd.testing.assert_frame_equal(df.reset_index(drop=True), expected_df.reset_index(drop=True))
        assert isinstance(df, pd.DataFrame), "O resultado deve ser um DataFrame pandas."
