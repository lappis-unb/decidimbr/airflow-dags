from unittest.mock import MagicMock, patch

import pandas as pd
import pytest
import requests
from sqlalchemy.exc import NoSuchTableError, SQLAlchemyError

from dags.data_ingestion.matomo_visits import (
    SCHEMA,
    TABLE_NAME,
    _fetch_visits_details,
    _write_data,
    delete_matching_rows_from_table,
)


@pytest.fixture
def context():
    return {"ds": "2023-06-01"}


@pytest.fixture
def sample_data():
    return pd.DataFrame({"serverDate": ["2023-06-01", "2023-06-02", "2023-06-01"]})


@patch("dags.data_ingestion.matomo_visits._get_matomo_credentials")
@patch("dags.data_ingestion.matomo_visits.requests.get")
def test_fetch_visits_details(mock_requests_get, mock_get_matomo_credentials, context):
    mock_get_matomo_credentials.return_value = ("1", "http://matomo.example.com", "fake_token")
    mock_requests_get.side_effect = [
        MagicMock(status_code=200, json=lambda: [{"visit": "data1"}]),
        MagicMock(status_code=200, json=lambda: [{"visit": "data2"}]),
        MagicMock(status_code=200, json=lambda: []),
    ]
    visits = _fetch_visits_details(limit=1, **context)
    assert mock_requests_get.call_count == 3
    assert visits == [{"visit": "data1"}, {"visit": "data2"}]


# @patch("dags.data_ingestion.matomo_visits._get_matomo_credentials")
# @patch("dags.data_ingestion.matomo_visits.requests.get")
# def test_fetch_visits_details_error(mock_requests_get, mock_get_matomo_credentials, context):
#    mock_get_matomo_credentials.return_value = ("1", "http://matomo.example.com", "fake_token")
#    mock_requests_get.return_value = MagicMock(status_code=500, text="Internal Server Error")
#    with pytest.raises(requests.exceptions.HTTPError) as excinfo:
#        _fetch_visits_details(limit=1, **context)
#    mock_requests_get.return_value.raise_for_status.assert_called_once()
#    assert "500 Server Error" in str(excinfo.value)


@patch("dags.data_ingestion.matomo_visits._get_matomo_credentials")
@patch("dags.data_ingestion.matomo_visits.requests.get")
def test_fetch_visits_details_error(mock_requests_get, mock_get_matomo_credentials, context):
    mock_get_matomo_credentials.return_value = ("1", "http://matomo.example.com", "fake_token")

    # Configura o mock para simular raise_for_status
    mock_response = MagicMock()
    mock_response.raise_for_status.side_effect = requests.exceptions.HTTPError("500 Server Error")
    mock_requests_get.return_value = mock_response

    # Testa a função e verifica a exceção
    with pytest.raises(requests.exceptions.HTTPError) as excinfo:
        _fetch_visits_details(limit=1, **context)

    mock_response.raise_for_status.assert_called_once()
    assert "500 Server Error" in str(excinfo.value)


@pytest.fixture
def mock_postgres_hook():
    with patch("dags.data_ingestion.matomo_visits.PostgresHook") as mock_hook:
        yield mock_hook


@pytest.fixture
def mock_delete_matching_rows():
    with patch("dags.data_ingestion.matomo_visits.delete_matching_rows_from_table") as mock_delete:
        yield mock_delete


@patch("dags.data_ingestion.matomo_visits.PostgresHook")
@patch("dags.data_ingestion.matomo_visits.delete_matching_rows_from_table")
@patch("dags.data_ingestion.matomo_visits.pd.DataFrame")
def test_write_data_success(mock_df_class, mock_delete_matching_rows, mock_postgres_hook):
    sample_data = [
        {"column1": "value1", "column2": {"subkey": "subvalue"}},
        {"column1": "value2", "column2": ["list", "of", "values"]},
    ]

    mock_engine = MagicMock()
    mock_postgres_hook.return_value.get_sqlalchemy_engine.return_value = mock_engine

    mock_df = MagicMock()
    mock_df.__getitem__.return_value.map.return_value = mock_df
    mock_df.columns = ["column1", "column2"]
    mock_df_class.return_value = mock_df

    result = _write_data(sample_data)
    mock_postgres_hook.assert_called_once_with(postgres_conn_id="pg_bp_analytics")
    mock_df_class.assert_called_once_with(sample_data)
    assert mock_df.__getitem__.call_count == len(mock_df.columns)
    mock_delete_matching_rows.assert_called_once_with(TABLE_NAME, SCHEMA, mock_engine, mock_df)
    mock_df.to_sql.assert_called_once_with(
        TABLE_NAME, con=mock_engine, schema=SCHEMA, if_exists="append", index=False
    )
    assert result is None


def test_write_data_empty(mock_postgres_hook, mock_delete_matching_rows):
    result = _write_data([])
    mock_postgres_hook.assert_not_called()
    mock_delete_matching_rows.assert_not_called()
    assert result is None


def test_write_data_no_such_table(mock_postgres_hook, mock_delete_matching_rows):
    sample_data = [
        {"column1": "value1", "column2": {"subkey": "subvalue"}},
        {"column1": "value2", "column2": ["list", "of", "values"]},
    ]

    mock_engine = MagicMock()
    mock_postgres_hook.return_value.get_sqlalchemy_engine.return_value = mock_engine
    mock_delete_matching_rows.side_effect = NoSuchTableError("No such table")

    with patch("dags.data_ingestion.matomo_visits.pd.DataFrame") as mock_df_class:
        mock_df = MagicMock()
        mock_df.columns = ["column1", "column2"]
        mock_df.__getitem__.side_effect = lambda col: MagicMock(map=MagicMock(return_value=mock_df))
        mock_df_class.return_value = mock_df

        result = _write_data(sample_data)
        mock_delete_matching_rows.assert_called_once_with(TABLE_NAME, SCHEMA, mock_engine, mock_df)
        mock_df.to_sql.assert_called_once_with(
            TABLE_NAME, con=mock_engine, schema=SCHEMA, if_exists="append", index=False
        )
        assert result is None


def test_write_data_no_such_table_prints_message(mock_postgres_hook, mock_delete_matching_rows, capsys):
    sample_data = [
        {"column1": "value1", "column2": {"subkey": "subvalue"}},
        {"column1": "value2", "column2": ["list", "of", "values"]},
    ]

    mock_engine = MagicMock()
    mock_postgres_hook.return_value.get_sqlalchemy_engine.return_value = mock_engine

    mock_delete_matching_rows.side_effect = NoSuchTableError(f"No such table: {SCHEMA}.{TABLE_NAME}")

    with patch("dags.data_ingestion.matomo_visits.pd.DataFrame") as mock_df_class:
        mock_df = MagicMock()
        mock_df.columns = ["column1", "column2"]
        mock_df.__getitem__.side_effect = lambda col: MagicMock(map=MagicMock(return_value=mock_df))
        mock_df_class.return_value = mock_df

        _write_data(sample_data)

        captured = capsys.readouterr()
        assert f"Table {SCHEMA}.{TABLE_NAME} não existe, criando uma nova tabela." in captured.out


def test_delete_matching_rows_success(sample_data):
    table_name = "matomo_detailed_visits"
    schema = "raw"
    engine = MagicMock()

    with patch("dags.data_ingestion.matomo_visits.sessionmaker") as mock_sessionmaker, patch(
        "dags.data_ingestion.matomo_visits.Table"
    ) as mock_table_class, patch("dags.data_ingestion.matomo_visits.MetaData") as mock_metadata_class:
        mock_session = MagicMock()
        mock_sessionmaker_instance = MagicMock(return_value=mock_session)
        mock_sessionmaker.return_value = mock_sessionmaker_instance
        mock_table = MagicMock()
        mock_table_class.return_value = mock_table
        mock_metadata = MagicMock()
        mock_metadata_class.return_value = mock_metadata
        mock_table.c = MagicMock()
        mock_table.c.serverDate = MagicMock()
        mock_where_clause = MagicMock()
        mock_table.c.serverDate.in_.return_value = mock_where_clause
        mock_delete_query = MagicMock()
        mock_table.delete.return_value = mock_delete_query
        mock_delete_query.where.return_value = mock_where_clause
        mock_result = MagicMock()
        mock_result.rowcount = 2
        mock_session.execute.return_value = mock_result
        delete_matching_rows_from_table(table_name, schema, engine, sample_data)
        mock_sessionmaker.assert_called_once_with(bind=engine)
        mock_sessionmaker_instance.assert_called_once()
        mock_metadata_class.assert_called_once_with(schema=schema)
        mock_table_class.assert_called_once_with(table_name, mock_metadata, autoload_with=engine)
        expected_to_delete = ["2023-06-01", "2023-06-02"]
        mock_table.c.serverDate.in_.assert_called_once_with(expected_to_delete)
        mock_table.delete.assert_called_once_with()
        mock_delete_query.where.assert_called_once_with(mock_where_clause)
        mock_session.execute.assert_called_once_with(mock_where_clause)
        mock_session.commit.assert_called_once()
        mock_session.close.assert_called_once()


def test_delete_matching_rows_empty_dataframe():
    table_name = "matomo_detailed_visits"
    schema = "raw"
    engine = MagicMock()
    empty_df = pd.DataFrame(columns=["serverDate"])

    with patch("dags.data_ingestion.matomo_visits.sessionmaker") as mock_sessionmaker, patch(
        "dags.data_ingestion.matomo_visits.Table"
    ) as mock_table_class, patch("dags.data_ingestion.matomo_visits.MetaData") as mock_metadata_class:
        delete_matching_rows_from_table(table_name, schema, engine, empty_df)
        mock_sessionmaker.assert_called_once_with(bind=engine)
        mock_metadata_class.assert_called_once_with(schema=schema)
        mock_table_class.assert_called_once_with(
            table_name, mock_metadata_class.return_value, autoload_with=engine
        )

        mock_sessionmaker.return_value.execute.assert_not_called()
        mock_sessionmaker.return_value.commit.assert_not_called()
        mock_sessionmaker.return_value.close.assert_not_called()


@pytest.fixture
def sample_data_delete():
    return pd.DataFrame({"serverDate": ["2023-06-01", "2023-06-02"]})


def test_delete_matching_rows_exception(sample_data_delete):
    table_name = "matomo_detailed_visits"
    schema = "raw"
    engine = MagicMock()
    with patch("dags.data_ingestion.matomo_visits.sessionmaker") as mock_sessionmaker, patch(
        "dags.data_ingestion.matomo_visits.Table"
    ) as mock_table_class, patch("dags.data_ingestion.matomo_visits.MetaData") as mock_metadata_class:

        mock_session = MagicMock()
        mock_sessionmaker_instance = MagicMock(return_value=mock_session)
        mock_sessionmaker.return_value = mock_sessionmaker_instance
        mock_table = MagicMock()
        mock_table_class.return_value = mock_table
        mock_metadata = MagicMock()
        mock_metadata_class.return_value = mock_metadata
        mock_table.c = MagicMock()
        mock_table.c.serverDate = MagicMock()
        mock_where_clause = MagicMock()
        mock_table.c.serverDate.in_.return_value = mock_where_clause
        mock_delete_query = MagicMock()
        mock_table.delete.return_value = mock_delete_query
        mock_delete_query.where.return_value = mock_where_clause
        mock_session.execute.side_effect = SQLAlchemyError("Erro durante a execução")

        with pytest.raises(SQLAlchemyError) as excinfo:
            delete_matching_rows_from_table(table_name, schema, engine, sample_data_delete)

        assert "Erro durante a execução" in str(excinfo.value)
        mock_session.commit.assert_not_called()
        mock_session.close.assert_called_once()
