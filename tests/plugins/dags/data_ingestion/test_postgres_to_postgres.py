from unittest.mock import MagicMock, patch

import pandas as pd
import pytest
from sqlalchemy import Column, Integer, String
from sqlalchemy.exc import NoSuchTableError

from dags.data_ingestion.postgres_to_postgres_cursor_ingestion import (
    _extract_data,
    _verify_columns,
    _write_data,
)


@patch("airflow.providers.postgres.hooks.postgres.PostgresHook")
@patch("pandas.read_sql")
def test_extract_data_success_case(
    mock_read_sql,
    mock_postgres_hook,
):

    mock_postgres_hook.get_connection.return_value = MagicMock()

    expected_data = pd.DataFrame(
        [
            {"id": 1, "name": "test"},
            {"id": 2, "name": "test2"},
        ]
    )
    mock_read_sql.return_value = expected_data

    extraction_info = {
        "extraction_schema": "public",
        "ingestion_type": "incremental",
        "incremental_filter": "id > 0",
    }

    df = _extract_data("test_table", extraction_info, "mock_conn_id", ssh_tunnel=True)

    assert isinstance(df, pd.DataFrame)
    assert len(df) == 2
    assert df.equals(expected_data)


@patch("airflow.providers.postgres.hooks.postgres.PostgresHook")
@patch("pandas.read_sql")
def test_extract_data_empty_table(mock_read_sql, mock_postgres_hook):

    mock_postgres_hook.get_connection.return_value = MagicMock()

    # DataFrame vazio
    mock_read_sql.return_value = pd.DataFrame()

    extraction_info = {
        "extraction_schema": "public",
        "ingestion_type": "incremental",
        "incremental_filter": "id > 0",
    }

    # Executa a função
    df = _extract_data("test_table", extraction_info, "mock_conn_id", ssh_tunnel=True)

    # Verificações
    assert isinstance(df, pd.DataFrame)
    assert df.empty
    mock_read_sql.assert_called_once()
    query_executed = mock_read_sql.call_args[0][0]
    assert "SELECT" in query_executed


@patch("airflow.providers.postgres.hooks.postgres.PostgresHook")
@patch("pandas.read_sql")
def test_extract_data_table_not_exists(mock_read_sql, mock_postgres_hook):

    mock_postgres_hook.get_connection.return_value = MagicMock()

    mock_read_sql.side_effect = Exception("Table does not exist")

    extraction_info = {
        "extraction_schema": "public",
        "ingestion_type": "incremental",
        "incremental_filter": "id > 0",
    }

    with pytest.raises(Exception, match="Table does not exist"):
        _extract_data("non_existing_table", extraction_info, "mock_conn_id", ssh_tunnel=True)


@patch("airflow.providers.postgres.hooks.postgres.PostgresHook")
@patch("pandas.read_sql")
def test_extract_data_exclude_columns(
    mock_read_sql,
    mock_postgres_hook,
):

    mock_postgres_hook.get_connection.return_value = MagicMock()

    expected_data = pd.DataFrame(
        [
            {"id": 1},
            {"id": 2},
        ]
    )
    mock_read_sql.return_value = expected_data

    extraction_info = {
        "extraction_schema": "public",
        "ingestion_type": "incremental",
        "incremental_filter": "id > 0",
        "exclude_columns": ["name"],
    }

    df = _extract_data("test_table", extraction_info, "mock_conn_id", ssh_tunnel=True)

    assert isinstance(df, pd.DataFrame)
    assert "name" not in df.columns
    assert len(df) == 2


@patch("airflow.providers.postgres.hooks.postgres.PostgresHook")
@patch("pandas.read_sql")
def test_extract_data_invalid_sql_transformations(
    mock_read_sql,
    mock_postgres_hook,
):

    mock_postgres_hook.get_connection.return_value = MagicMock()

    extraction_info = {
        "extraction_schema": "public",
        "ingestion_type": "incremental",
        "incremental_filter": "id > 0",
        "sql_transformations": "DROP TABLE test_table",
    }

    with pytest.raises(
        ValueError,
        match="O 'sql_transformations' não deve conter cláusulas 'FROM',"
        "'JOIN' ou outras operações não permitidas.",
    ):
        _extract_data("test_table", extraction_info, "mock_conn_id", ssh_tunnel=True)


@patch("airflow.providers.postgres.hooks.postgres.PostgresHook")
@patch("pandas.read_sql")
def test_extract_data_null_values(
    mock_read_sql,
    mock_postgres_hook,
):

    mock_postgres_hook.get_connection.return_value = MagicMock()

    expected_data = pd.DataFrame(
        [
            {"id": 1, "name": None},
            {"id": 2, "name": "test2"},
        ]
    )
    mock_read_sql.return_value = expected_data

    extraction_info = {
        "extraction_schema": "public",
        "ingestion_type": "incremental",
        "incremental_filter": "id > 0",
    }

    df = _extract_data("test_table", extraction_info, "mock_conn_id", ssh_tunnel=True)

    assert isinstance(df, pd.DataFrame)
    assert df["name"].isnull().any()


@patch("airflow.providers.postgres.hooks.postgres.PostgresHook")
def test_verify_columns_add_missing_columns(
    mock_postgres_hook,
):

    # Mocks do túnel e engines
    mock_origin_engine = MagicMock()
    mock_dest_engine = MagicMock()
    mock_postgres_hook.return_value.get_sqlalchemy_engine.side_effect = (mock_origin_engine, mock_dest_engine)

    # Mock das tabelas de origem e destino
    origin_columns = [
        Column("id", Integer),
        Column("name", String),
    ]
    dest_columns = [
        Column("id", Integer),  # 'name' está ausente
    ]

    # Mock das tabelas para autoload
    def mock_table(name, metadata, autoload_with):
        if metadata.bind == mock_origin_engine:
            return MagicMock(columns=origin_columns)
        elif metadata.bind == mock_dest_engine:
            return MagicMock(columns=dest_columns)

    with patch("sqlalchemy.Table", side_effect=mock_table):
        # Configuração dos parâmetros de extração
        extraction_info = {
            "extraction_schema": "public",
            "destination_schema": "public",
        }

        # Mock do método connect do dest_engine
        mock_dest_engine.connect.return_value.__enter__.return_value = MagicMock()

        # Executa a função
        _verify_columns("test_table", extraction_info, "origin_conn", "dest_conn", ssh_tunnel=True)

        # Verifica que a query ALTER TABLE foi chamada para adicionar colunas ausentes
        alter_query = "ALTER TABLE public.test_table ADD COLUMN name VARCHAR"
        mock_dest_engine.connect.return_value.__enter__.return_value.execute.assert_any_call(alter_query)

        # Verifica se o túnel foi fechado


@patch("airflow.providers.postgres.hooks.postgres.PostgresHook")
def test_verify_columns_table_does_not_exist(
    mock_postgres_hook,
):
    # Mocks do túnel e engines
    mock_origin_engine = MagicMock()
    mock_postgres_hook.return_value.get_sqlalchemy_engine.return_value = MagicMock()
    with pytest.raises(NoSuchTableError):
        # Simula que a tabela de destino não existe
        def mock_table(name, metadata, autoload_with):
            if metadata.bind == mock_origin_engine:
                return MagicMock(columns=[Column("id", Integer), Column("name", String)])
            else:
                raise NoSuchTableError

        with patch("sqlalchemy.Table", side_effect=mock_table):
            # Configuração dos parâmetros de extração
            extraction_info = {
                "extraction_schema": "public",
                "destination_schema": "public",
            }

            # Executa a função
            _verify_columns("test_table", extraction_info, "origin_conn", "dest_conn", ssh_tunnel=True)

            # Verifica que o túnel foi fechado


@patch("airflow.providers.postgres.hooks.postgres.PostgresHook")
def test_verify_columns_no_missing_columns(
    mock_postgres_hook,
):
    # Mocks do túnel e engines
    mock_dest_engine = MagicMock()
    mock_postgres_hook.return_value.get_sqlalchemy_engine.return_value = mock_dest_engine

    # Simula tabelas com colunas idênticas
    columns = [Column("id", Integer), Column("name", String)]

    def mock_table(name, metadata, autoload_with):
        return MagicMock(columns=columns)

    with patch("sqlalchemy.Table", side_effect=mock_table):
        extraction_info = {
            "extraction_schema": "public",
            "destination_schema": "public",
        }

        mock_dest_engine.connect.return_value.__enter__.return_value.execute = MagicMock()

        _verify_columns("test_table", extraction_info, "origin_conn", "dest_conn", ssh_tunnel=True)

        # Verifica que nenhuma query foi executada
        mock_dest_engine.connect.return_value.__enter__.return_value.execute.assert_not_called()

        # Verifica se o túnel foi fechado


@patch("airflow.providers.postgres.hooks.postgres.PostgresHook")
@patch("sqlalchemy.orm.sessionmaker")
def test_write_data_empty_dataframe(mock_sessionmaker, mock_postgres_hook):
    # DataFrame vazio
    df = pd.DataFrame()

    mock_engine = MagicMock()
    mock_postgres_hook.return_value.get_sqlalchemy_engine.return_value = mock_engine

    result = _write_data(df, "test_table", {"destination_schema": "public"}, "test_conn")

    # Verifica que nada foi escrito
    mock_postgres_hook.return_value.get_sqlalchemy_engine.assert_not_called()
    mock_sessionmaker.assert_not_called()
    assert result is None


@patch("airflow.providers.postgres.hooks.postgres.PostgresHook")
@patch("sqlalchemy.orm.sessionmaker")
def test_write_data_incremental_with_deletion(mock_sessionmaker, mock_postgres_hook):
    df = pd.DataFrame({"id": [1, 2], "name": ["Alice", "Bob"]})

    mock_engine = MagicMock()
    mock_postgres_hook.return_value.get_sqlalchemy_engine.return_value = mock_engine

    mock_session = MagicMock()
    mock_sessionmaker.return_value.return_value = mock_session

    mock_table = MagicMock()
    mock_table.c = MagicMock()
    mock_table.c.id = MagicMock()

    with patch("sqlalchemy.Table", return_value=mock_table):
        # Executa a função
        _write_data(
            df,
            "test_table",
            {"destination_schema": "public", "ingestion_type": "incremental"},
            "test_conn",
        )

        # Verifica que a deleção foi executada
        mock_session.execute.assert_called()
        mock_session.commit.assert_called()
        mock_session.close.assert_called()


@patch("airflow.providers.postgres.hooks.postgres.PostgresHook")
@patch("sqlalchemy.orm.sessionmaker")
def test_write_data_complete_deletion(mock_sessionmaker, mock_postgres_hook):
    df = pd.DataFrame({"id": [1, 2], "name": ["Alice", "Bob"]})

    mock_engine = MagicMock()
    mock_postgres_hook.return_value.get_sqlalchemy_engine.return_value = mock_engine

    mock_session = MagicMock()
    mock_sessionmaker.return_value.return_value = mock_session

    _write_data(
        df,
        "test_table",
        {"destination_schema": "public", "ingestion_type": "complete"},
        "test_conn",
    )

    # Verifica que a tabela foi truncada
    mock_session.execute.assert_called_once()
    mock_session.commit.assert_called_once()
    mock_session.close.assert_called_once()


@patch("airflow.providers.postgres.hooks.postgres.PostgresHook")
@patch("sqlalchemy.orm.sessionmaker")
def test_write_data_table_does_not_exist(mock_sessionmaker, mock_postgres_hook):
    df = pd.DataFrame({"id": [1, 2], "name": ["Alice", "Bob"]})

    mock_engine = MagicMock()
    mock_postgres_hook.return_value.get_sqlalchemy_engine.return_value = mock_engine

    mock_session = MagicMock()
    mock_sessionmaker.return_value.return_value = mock_session

    with patch("sqlalchemy.Table", side_effect=NoSuchTableError):
        _write_data(
            df,
            "nonexistent_table",
            {"destination_schema": "public", "ingestion_type": "incremental"},
            "test_conn",
        )

        # Verifica que a exceção foi tratada e que a sessão foi encerrada
        mock_session.close.assert_called_once()
