import pytest

from automation.generate_dbt_dag import (
    create_dependencies,
    generate_airflow_schedule,
    generate_airflow_task,
    get_models_dependencies,
    parse_manifest,
)


# Testes para a função create_dependencies
def test_create_dependencies_no_tests():
    node = "model_a"
    dependencies = []
    result = create_dependencies(node, dependencies)
    expected = ["    model_a_task >> end_task"]
    assert result == expected


def test_create_dependencies_one_test():
    node = "model_a"
    dependencies = ["test_b"]
    result = create_dependencies(node, dependencies)
    expected = ["    model_a_task >> test_b_task >> end_task"]
    assert result == expected


def test_create_dependencies_multiple_tests():
    node = "model_a"
    dependencies = ["test_b", "test_c"]
    result = create_dependencies(node, dependencies)
    expected = ["    model_a_task >> test_b_task >> end_task", "    model_a_task >> test_c_task >> end_task"]
    assert result == expected


def test_create_dependencies_custom_indentation():
    node = "model_a"
    dependencies = ["test_b"]
    result = create_dependencies(node, dependencies, indentation="\t")
    expected = ["\tmodel_a_task >> test_b_task >> end_task"]
    assert result == expected


def test_create_dependencies_empty_custom_indentation():
    node = "model_a"
    dependencies = []
    result = create_dependencies(node, dependencies, indentation="\t")
    expected = ["\tmodel_a_task >> end_task"]
    assert result == expected


def test_create_dependencies_invalid_node_type():
    node = 123
    dependencies = []
    with pytest.raises(TypeError):
        create_dependencies(node, dependencies)


def test_create_dependencies_invalid_dependencies_type():
    node = "model_a"
    dependencies = "test_b"
    with pytest.raises(TypeError):
        create_dependencies(node, dependencies)


def test_create_dependencies_invalid_dependency_element_type():
    node = "model_a"
    dependencies = ["test_b", 123]
    with pytest.raises(TypeError):
        create_dependencies(node, dependencies)


def test_create_dependencies_invalid_indentation_type():
    node = "model_a"
    dependencies = ["test_b"]
    indentation = 4
    with pytest.raises(TypeError):
        create_dependencies(node, dependencies, indentation=indentation)


# Testes para a função generate_airflow_schedule
def test_generate_airflow_schedule_no_dependencies():
    model_dependecies = []
    nodes_type_map = {"model1": "model", "model2": "model"}
    result = generate_airflow_schedule(model_dependecies, nodes_type_map)
    assert result == "'@daily'"


def test_generate_airflow_schedule_all_valid_models():
    model_dependecies = ["model1", "model2"]
    nodes_type_map = {"model1": "model", "model2": "model"}
    result = generate_airflow_schedule(model_dependecies, nodes_type_map)
    expected = "[Dataset('model1_model'), Dataset('model2_model')]"
    assert result == expected


def test_generate_airflow_schedule_partial_valid_models():
    model_dependecies = ["model1", "model3"]
    nodes_type_map = {"model1": "model", "model2": "model"}
    result = generate_airflow_schedule(model_dependecies, nodes_type_map)
    expected = "[Dataset('model1_model'), Dataset('model3')]"
    assert result == expected


def test_generate_airflow_schedule_empty_dependencies():
    model_dependecies = []
    nodes_type_map = {}
    result = generate_airflow_schedule(model_dependecies, nodes_type_map)
    assert result == "'@daily'"


def test_generate_airflow_schedule_invalid_models():
    model_dependecies = ["nonexistent_model"]
    nodes_type_map = {"model1": "model", "model2": "model"}
    result = generate_airflow_schedule(model_dependecies, nodes_type_map)
    expected = "[Dataset('nonexistent_model')]"
    assert result == expected


# Testes para a função generate_airflow_task
@pytest.mark.parametrize(
    "node_type, node_name, expected_command",
    [
        ("model", "example_model", "dbt run --select example_model"),
        ("test", "example_test", "dbt test --select example_test"),
    ],
)
def test_generate_airflow_task_valid_inputs(node_type, node_name, expected_command, monkeypatch):
    monkeypatch.setenv("DBT_POSTGRES_HOST", "localhost")
    result = generate_airflow_task(node_type, node_name, "/path/to/dbt_project", "/path/to/dbt_profile")
    assert expected_command in result
    assert "BashOperator" in result, "O código gerado não contém BashOperator."


@pytest.mark.parametrize(
    "node_type, node_name, dbt_project_path, dbt_profile_path, expected_exception",
    [
        ("invalid_type", "example_model", "/valid/path", "/valid/path", KeyError),
        ("model", None, "/valid/path", "/valid/path", TypeError),
        ("model", "example_model", None, "/valid/path", TypeError),
    ],
)
def test_generate_airflow_task_invalid_inputs(
    node_type, node_name, dbt_project_path, dbt_profile_path, expected_exception, monkeypatch
):
    monkeypatch.setenv("DBT_POSTGRES_HOST", "localhost")
    with pytest.raises(expected_exception):
        generate_airflow_task(node_type, node_name, dbt_project_path, dbt_profile_path)


def test_generate_airflow_task_output_format(monkeypatch):
    monkeypatch.setenv("DBT_POSTGRES_HOST", "localhost")
    result = generate_airflow_task("model", "example_model", "/path/to/dbt_project", "/path/to/dbt_profile")
    assert "BashOperator" in result
    assert "task_id='run_example_model'" in result
    assert "bash_command='rm -r" in result


# Testes para a função get_models_dependencies
@pytest.fixture
def valid_data():
    upstreams = {
        "model.a": ["model.b"],
        "model.b": [],
        "test.t1": ["model.a"],
        "test.t2": ["model.b"],
    }
    nodes_type_map = {
        "model.a": "model",
        "model.b": "model",
        "test.t1": "test",
        "test.t2": "test",
    }
    datasets_map = {
        "model.a": ["dataset.d1"],
        "model.b": ["dataset.d2"],
    }
    return upstreams, nodes_type_map, datasets_map


@pytest.fixture
def invalid_data():
    upstreams = {
        "model.a": ["model.b"],
        "test.t1": ["model.a"],
    }
    nodes_type_map = {
        "model.a": "model",
        "test.t1": "test",
    }
    datasets_map = {}
    return upstreams, nodes_type_map, datasets_map


def test_valid_dependencies(valid_data):
    upstreams, nodes_type_map, datasets_map = valid_data
    result = get_models_dependencies(upstreams, nodes_type_map, datasets_map)
    expected = {
        "model.a": {
            "tests_dependecies": ["test.t1"],
            "model_dependecies": ["model.b", "dataset.d1"],
        },
        "model.b": {
            "tests_dependecies": ["test.t2"],
            "model_dependecies": ["dataset.d2"],
        },
    }
    assert result == expected


def test_empty_datasets(valid_data):
    upstreams, nodes_type_map, _ = valid_data
    datasets_map = {}
    result = get_models_dependencies(upstreams, nodes_type_map, datasets_map)
    expected = {
        "model.a": {
            "tests_dependecies": ["test.t1"],
            "model_dependecies": ["model.b"],
        },
        "model.b": {
            "tests_dependecies": ["test.t2"],
            "model_dependecies": [],
        },
    }
    assert result == expected


def test_no_upstreams():
    upstreams = {}
    nodes_type_map = {}
    datasets_map = {}
    result = get_models_dependencies(upstreams, nodes_type_map, datasets_map)
    assert result == {}


def test_missing_tests():
    upstreams = {"model.a": ["model.b"]}
    nodes_type_map = {"model.a": "model", "model.b": "model"}
    datasets_map = {}
    result = get_models_dependencies(upstreams, nodes_type_map, datasets_map)
    expected = {
        "model.a": {
            "tests_dependecies": [],
            "model_dependecies": ["model.b"],
        }
    }
    assert result == expected


def test_missing_node_in_map(invalid_data):
    upstreams, nodes_type_map, datasets_map = invalid_data
    with pytest.raises(KeyError, match="model.b"):
        get_models_dependencies(upstreams, nodes_type_map, datasets_map)


def test_invalid_types():
    upstreams = "invalid_upstreams"
    nodes_type_map = "invalid_nodes_type_map"
    datasets_map = "invalid_datasets_map"
    with pytest.raises(TypeError):
        get_models_dependencies(upstreams, nodes_type_map, datasets_map)


def test_partial_nodes_map():
    upstreams = {"model.a": ["model.b"], "test.t1": ["model.a"]}
    nodes_type_map = {"model.a": "model"}
    datasets_map = {}
    with pytest.raises(KeyError, match="model.b"):
        get_models_dependencies(upstreams, nodes_type_map, datasets_map)


def test_empty_inputs():
    result = get_models_dependencies({}, {}, {})
    assert result == {}


def test_no_models():
    upstreams = {"test.t1": ["model.a"]}
    nodes_type_map = {"test.t1": "test"}
    datasets_map = {}
    with pytest.raises(KeyError, match="model.a"):
        get_models_dependencies(upstreams, nodes_type_map, datasets_map)


# Testes para a função parse_manifest
@pytest.fixture
def example_manifest():
    return {
        "nodes": {
            "model.model_a": {
                "name": "model_a",
                "resource_type": "model",
                "depends_on": {"nodes": ["model.model_b", "model.model_c"]},
                "meta": {"datasets_trigger": "dataset1,dataset2"},
            },
            "model.model_b": {
                "name": "model_b",
                "resource_type": "model",
                "depends_on": {"nodes": []},
                "meta": {},
            },
            "model.model_c": {
                "name": "model_c",
                "resource_type": "model",
                "depends_on": {"nodes": []},
                "meta": {},
            },
        }
    }


def test_nodes_type_map(example_manifest):
    _, nodes_type_map, _ = parse_manifest(example_manifest)
    assert nodes_type_map == {"model_a": "model", "model_b": "model", "model_c": "model"}


def test_datasets_map(example_manifest):
    _, _, datasets_map = parse_manifest(example_manifest)
    assert datasets_map == {"model_a": ["dataset1", "dataset2"]}


def test_upstreams(example_manifest):
    upstreams, _, _ = parse_manifest(example_manifest)
    assert upstreams == {"model_a": ["model_b", "model_c"], "model_b": [], "model_c": []}


def test_parse_manifest_full(example_manifest):
    upstreams, nodes_type_map, datasets_map = parse_manifest(example_manifest)
    assert upstreams == {"model_a": ["model_b", "model_c"], "model_b": [], "model_c": []}
    assert nodes_type_map == {"model_a": "model", "model_b": "model", "model_c": "model"}
    assert datasets_map == {"model_a": ["dataset1", "dataset2"]}


def test_missing_nodes_key():
    manifest = {}
    with pytest.raises(KeyError):
        parse_manifest(manifest)


def test_missing_resource_type():
    manifest = {"nodes": {"model.model_a": {"name": "model_a", "depends_on": {"nodes": []}, "meta": {}}}}
    with pytest.raises(KeyError):
        parse_manifest(manifest)


def test_invalid_datasets_trigger_type():
    manifest = {
        "nodes": {
            "model.model_a": {
                "name": "model_a",
                "resource_type": "model",
                "depends_on": {"nodes": []},
                "meta": {"datasets_trigger": 123},
            }
        }
    }
    with pytest.raises(AssertionError):
        parse_manifest(manifest)


def test_invalid_dependencies():
    manifest = {
        "nodes": {
            "model.model_a": {
                "name": "model_a",
                "resource_type": "model",
                "depends_on": {"nodes": ["model.invalid_node"]},
                "meta": {},
            }
        }
    }
    result, _, _ = parse_manifest(manifest)
    assert result == {"model_a": []}


def test_empty_manifest():
    manifest = {"nodes": {}}
    upstreams, nodes_type_map, datasets_map = parse_manifest(manifest)
    assert upstreams == {}
    assert nodes_type_map == {}
    assert datasets_map == {}


def test_missing_node_name():
    manifest = {
        "nodes": {"model.unnamed_node": {"resource_type": "model", "depends_on": {"nodes": []}, "meta": {}}}
    }
    with pytest.raises(KeyError):
        parse_manifest(manifest)


def test_missing_depends_on():
    manifest = {"nodes": {"model.model_a": {"name": "model_a", "resource_type": "model", "meta": {}}}}
    result, _, _ = parse_manifest(manifest)
    assert result == {"model_a": []}
