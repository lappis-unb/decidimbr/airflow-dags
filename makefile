ifneq (,$(wildcard ./.env))
    include .env
    export
endif

build-testing-img:
	docker rmi airflow-testing-lappis:latest
	docker build -t airflow-testing-lappis:latest .

run-pytest:
	make build-testing-img
	docker run airflow-testing-lappis:latest "pytest"

run-linters:
	make build-testing-img
	docker run airflow-testing-lappis:latest "black ."
	docker run airflow-testing-lappis:latest "ruff ."

run-static-analyzer:
	make build-testing-img
	docker run airflow-testing-lappis:latest "mypy ."

lint:
	black .
	ruff check --fix --unsafe-fixes .

start-bastion:
	ssh -l ${BASTION_USER} ${BASTION_HOST} -p ${BASTION_PORT} -N -C -L "${DBT_POSTGRES_PORT}:${BASTION_DB_HOST}:${BASTION_DB_PORT}"

compile-dbt:
	cd plugins/dbt_pg_project && dbt compile
	python automation/generate_dbt_dag.py --manifest_path plugins/dbt_pg_project/target/manifest.json --project_path plugins/dbt_pg_project/ --profile_path plugins/dbt_pg_project/ --dag_folder_path dbt/