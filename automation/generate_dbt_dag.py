import argparse
import json
from pathlib import Path


def parse_args():
    """
    Analisa os argumentos passados na linha de comando.

    Retorna:
        argparse.Namespace: Objeto contendo os argumentos fornecidos.
    """
    parser = argparse.ArgumentParser(description="Process dbt manifest and generate Airflow DAG.")
    parser.add_argument(
        "--manifest_path",
        default="dbt_pg_project/target/manifest.json",
        required=False,
        help="Path to the dbt manifest file",
    )
    parser.add_argument(
        "--project_path", default="dbt_pg_project", required=False, help="Path to the dbt project"
    )
    parser.add_argument(
        "--profile_path", default="dbt_pg_project", required=False, help="Path to the dbt profile"
    )
    parser.add_argument(
        "--dag_folder_path",
        default="dags/dbt/",
        required=False,
        help="Folder path where generated DAGs will be saved",
    )
    return parser.parse_args()


def get_manifest(manifest_path):
    """
    Carrega e retorna o conteúdo do manifesto dbt.

    Args:
        manifest_path (str): Caminho para o arquivo JSON do manifesto.

    Retorna:
        dict: Conteúdo do manifesto dbt.
    """
    with open(manifest_path) as f:
        manifest = json.load(f)
    return manifest


def generate_airflow_task(node_type, node_name, dbt_project_path, dbt_profile_path, indentation="    "):
    """
    Gera a string de código para uma tarefa do Airflow baseada em um nó do dbt.

    Args:
        node_type (str): Tipo do nó ('model' ou 'test').
        node_name (str): Nome do nó.
        dbt_project_path (str): Caminho para o projeto dbt.
        dbt_profile_path (str): Caminho para o perfil dbt.
        indentation (str, opcional): Indentação utilizada no código gerado. Padrão é 4 espaços.

    Retorna:
        str: Código Python representando a tarefa do Airflow.
    """
    valid_node_types = {"model", "test"}

    if node_type not in valid_node_types:
        raise KeyError(f"Invalid node_type: {node_type}. Must be one of {valid_node_types}.")
    if not node_name or not isinstance(node_name, str):
        raise TypeError("node_name must be a non-empty string.")
    if not dbt_project_path or not isinstance(dbt_project_path, str):
        raise TypeError("dbt_project_path must be a non-empty string.")
    if not dbt_profile_path or not isinstance(dbt_profile_path, str):
        raise TypeError("dbt_profile_path must be a non-empty string.")

    type2command = {"model": "run", "test": "test"}
    dbt_command = type2command[node_type]
    task_id = f"{dbt_command}_{node_name}"
    k8s_dbt_project_path = f"/tmp/dbt_{dbt_command}_{node_name}"
    task = f"""
{indentation}{node_name}_task = BashOperator(
{indentation}{indentation}task_id='{task_id}',
{indentation}{indentation}bash_command='rm -r {k8s_dbt_project_path} || true \\
&& cp -r {dbt_project_path} {k8s_dbt_project_path} \\
&& cd {k8s_dbt_project_path} \\
&& dbt deps && dbt {dbt_command} --select {node_name} \\
&& rm -r {k8s_dbt_project_path}',
{indentation}{indentation}env={{
{indentation}{indentation}{indentation}'DBT_POSTGRES_HOST': Variable.get("bp_dw_pg_host"),
{indentation}{indentation}{indentation}'DBT_POSTGRES_USER': Variable.get("bp_dw_pg_user"),
{indentation}{indentation}{indentation}'DBT_POSTGRES_PASSWORD': Variable.get("bp_dw_pg_password"),
{indentation}{indentation}{indentation}'DBT_POSTGRES_ENVIRONMENT': Variable.get("bp_dw_pg_environment"),
{indentation}{indentation}{indentation}'DBT_POSTGRES_PORT': Variable.get("bp_dw_pg_port"),
{indentation}{indentation}{indentation}'DBT_POSTGRES_DATABASE': Variable.get("bp_dw_pg_db"),
{indentation}{indentation}}},
{indentation}{indentation}append_env=True
{indentation})"""
    return task


def generate_airflow_schedule(model_dependecies, nodes_type_map):
    """
    Gera a string de agendamento do Airflow com base nas dependências dos modelos.

    Args:
        model_dependecies (list): Lista de dependências do modelo.
        nodes_type_map (dict): Mapeamento de nós para seus tipos.

    Retorna:
        str: Configuração de agendamento do Airflow.
    """
    if not model_dependecies:
        return "'@daily'"

    model_dependecies = [
        f"{model}_model" if model in nodes_type_map else f"{model}" for model in model_dependecies
    ]

    schedule_dataset = ", ".join([f"Dataset('{dep}')" for dep in model_dependecies])
    return f"[{schedule_dataset}]"


def generate_airflow_dag_id(node, nodes_type_map):
    """
    Gera um identificador único para um DAG do Airflow.

    Args:
        node (str): Nome do nó.
        nodes_type_map (dict): Mapeamento de nós para seus tipos.

    Retorna:
        str: Identificador único do DAG.
    """
    return f"run_{nodes_type_map[node]}__{node}"


def create_dependencies(node, model_tests_dependecies: list, indentation="    "):
    """
    Cria as dependências entre tarefas do Airflow.

    Args:
        node (str): Nome do nó.
        model_tests_dependecies (list): Lista de dependências do modelo.
        indentation (str, opcional): Indentação do código gerado. Padrão é 4 espaços.

    Retorna:
        list: Lista de strings representando as dependências entre tarefas.
    """
    if not isinstance(node, str):
        raise TypeError("Parameter 'node' must be a string.")
    if not isinstance(model_tests_dependecies, list):
        raise TypeError("Parameter 'model_tests_dependecies' must be a list.")
    if not all(isinstance(dep, str) for dep in model_tests_dependecies):
        raise TypeError("All elements in 'model_tests_dependecies' must be strings.")
    if not isinstance(indentation, str):
        raise TypeError("Parameter 'indentation' must be a string.")

    if not model_tests_dependecies:
        return [f"{indentation}{node}_task >> end_task"]

    dependencies = []
    for test_dependency in model_tests_dependecies:
        dependencies.append(f"{indentation}{node}_task >> {test_dependency}_task >> end_task")
    return dependencies


def parse_manifest(manifest):
    """
    Analisa o manifesto do dbt e extrai informações sobre os nós, seus tipos e dependências.

    Args:
        Dicionário representando o manifesto do dbt.

    Retorna:
        Três dicionários contendo as dependências entre nós, tipos de nós e os datasets acionadores.
    """
    nodes = manifest["nodes"]
    nodes_type_map = {node["name"]: node["resource_type"] for _, node in nodes.items()}

    datasets_map = {}
    for _, node in nodes.items():
        if "datasets_trigger" in node["meta"]:
            triggers = node["meta"]["datasets_trigger"]
            if isinstance(triggers, str):
                triggers = triggers.split(",")
            assert isinstance(triggers, list)
            datasets_map[node["name"]] = triggers

    upstreams = {}
    for _, node in nodes.items():
        node_name = node["name"]
        depends_on = node.get("depends_on", {})
        node_dependencies = depends_on.get("nodes", [])
        node_dependencies = [
            nodes[dep]["name"] for dep in node_dependencies if dep.startswith("model") and dep in nodes
        ]
        upstreams[node_name] = node_dependencies

    return upstreams, nodes_type_map, datasets_map


def get_models_dependencies(upstreams, nodes_type_map, datasets_map):
    """
    Obtém as dependências entre os modelos e testes no fluxo de trabalho do dbt.

    Args:
        upstreams: Dicionário mapeando nós para suas dependências.
        nodes_type_map: Dicionário mapeando nós para seus tipos.
        datasets_map: Dicionário mapeando modelos para datasets acionadores.

    Retorna:
        Dicionário com dependências dos modelos e testes.
    """
    if (
        not isinstance(upstreams, dict)
        or not isinstance(nodes_type_map, dict)
        or not isinstance(datasets_map, dict)
    ):
        raise TypeError("All inputs must be dictionaries")

    for node, deps in upstreams.items():
        if node not in nodes_type_map:
            raise KeyError(f"Node '{node}' is missing from 'nodes_type_map'")
        if not isinstance(deps, list):
            raise TypeError(f"Dependencies for node '{node}' must be a list")
        for dep in deps:
            if dep not in nodes_type_map:
                raise KeyError(f"Dependency '{dep}' is missing from 'nodes_type_map'")
        if not isinstance(nodes_type_map[node], str):
            raise TypeError(f"Node type for '{node}' must be a string")

    tests_dependecies = {}
    for node, upstream in upstreams.items():
        if nodes_type_map[node] != "test":
            continue
        for dep in upstream:
            _map = tests_dependecies.get(dep, [])
            _map.append(node)
            tests_dependecies[dep] = _map

    models_dependecies = {
        node: [*deps, *datasets_map.get(node, [])]
        for node, deps in upstreams.items()
        if nodes_type_map[node] == "model"
    }

    dependencies = {
        node: {
            "tests_dependecies": tests_dependecies.get(node, []),
            "model_dependecies": models_dependecies[node],
        }
        for node in models_dependecies
    }

    return dependencies


def generate_airflow_dags(dag_folder_path, manifest, dbt_project_path, dbt_profile_path):
    """
    Gera DAGs do Apache Airflow com base no manifesto do dbt.

    Args:
        dag_folder_path: Caminho para a pasta onde os DAGs serão salvos.
        manifest: Dicionário contendo o manifesto do dbt.
        dbt_project_path: Caminho para o projeto dbt.
        dbt_profile_path: Caminho para o perfil do dbt.
    """
    if not isinstance(manifest, dict):
        raise ValueError("The provided manifest must be a valid dictionary.")

    try:
        upstreams, nodes_type_map, datasets_map = parse_manifest(manifest)
    except KeyError as e:
        raise KeyError(f"Error processing the manifest. Missing key: {e}") from e

    models_dependencies = get_models_dependencies(upstreams, nodes_type_map, datasets_map)

    dag_path = Path(dag_folder_path)
    try:
        dag_path.mkdir(parents=True, exist_ok=True)
    except Exception as e:
        raise OSError(f"Error creating the output folder {dag_folder_path}: {e}") from e

    template_path = Path(__file__).parent.joinpath("./dag_template.txt")
    if not template_path.exists():
        raise FileNotFoundError(f"DAG template not found at: {template_path}")

    for node, dependencies in models_dependencies.items():
        if node not in nodes_type_map:
            raise ValueError(f"The node '{node}' is not present in the nodes type map (nodes_type_map).")

        try:
            tasks_str = "\n".join(
                [
                    generate_airflow_task(
                        nodes_type_map[dbt_node], dbt_node, dbt_project_path, dbt_profile_path
                    )
                    for dbt_node in [node, *dependencies.get("tests_dependecies", [])]
                ]
            )

            tests_dependecies = create_dependencies(
                node, dependencies.get("tests_dependecies", []), indentation="    "
            )
            dependencies_str = "\n".join(tests_dependecies) if tests_dependecies else ""

            dag_name = generate_airflow_dag_id(node, nodes_type_map)

            with open(template_path) as dag_template_file:
                dag_content = dag_template_file.read()

            if not dag_content:
                raise ValueError("The DAG template content is empty.")

            with open(dag_path.joinpath(f"{dag_name}.py"), "w") as f:
                f.write(
                    dag_content.format(
                        dag_id=dag_name,
                        tasks_str=tasks_str,
                        node_type=nodes_type_map[node],
                        dependencies_str=dependencies_str,
                        dag_trigger=generate_airflow_schedule(
                            dependencies.get("model_dependecies", []), nodes_type_map
                        ),
                        outlet_dataset=f"{node}_model",
                    )
                )

        except Exception as e:
            raise RuntimeError(f"Error generating DAG for node '{node}': {e}") from e


if __name__ == "__main__":
    args = parse_args()
    manifest = get_manifest(args.manifest_path)
    generate_airflow_dags(args.dag_folder_path, manifest, args.project_path, args.profile_path)
